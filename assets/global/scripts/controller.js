var httpBaseRequest = "http://localhost:8080/lazi.api/crmservice/"; // main options
var baseURL = "http://localhost:8080/CMS_HTML/";
var ckFinderUrl = "http://localhost:8080/CKFinderJava/ckfinder/ckfinder.js";

//var httpBaseRequest = "http://inet.legasoft.com:8080/api/crmservice/"; // main options
//var baseURL = "http://inet.legasoft.com:8080/cms/";
//var ckFinderUrl = "http://inet.legasoft.com:8080/CKFinderJava/ckfinder/ckfinder.js";

var listNewsParent = "danhmucTinTuc";
var bussinessCode = "01";

if (localStorage.loginSession === undefined || localStorage.loginSession <= new Date().getTime()) {
    // window.location.href = "login.html";

}

function checkRegexShortUrl(input) {
    input = "" + input;
    var regexObject = /[0-z'-]/g;
    var result = input.match(regexObject);
    console.log(result);
    if (result === null) {
        return false;
    }
    return input.length === result.length;
}

function removeCharacter(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}

String.prototype.replaceAll = function(str1, str2, ignore) 
{
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
} 


$(window).on('hashchange load', function () {
        urlController();
});

$("li").on("click",function(){
  if ($(this).parent().attr('class') == "page-sidebar-menu"){
      $(this).parent().find(".open").removeClass();
    $(this).addClass("open");
  }
});



function urlController(){
    var url = $(location).attr('href');
    var action = url.split("#")[1];
	if (action === undefined) action = "home";
    doesFileExist(action + ".html", function(data){
            if (data){
                 $("#contentPage").load(action + ".html");
            } else {
                
               $("#contentPage").load("404.html");
            }
        }); 
}

function doesFileExist(urlToFile, callback) {
   $.ajax({
    url: baseURL + urlToFile,
    type:'HEAD',
        error: function()
        {
            callback(false);
        },
        success: function()
        {
            callback(true);
        }
    });
}

jQuery.cachedScript = function( url, options ) {
  // Allow user to set any option except for dataType, cache, and url
  options = $.extend( options || {}, {
    dataType: "script",
    cache: true,
    url: url
  });
 
  // Use $.ajax() since it is more flexible than $.getScript
  // Return the jqXHR object so we can chain callbacks
  return jQuery.ajax( options );
};


$.cachedScript(ckFinderUrl).done(function( script, textStatus ) {
  console.log( textStatus );
});

function isEmpty(val)
{
    if (val === '' || val === null || val === undefined || val == null) {
        return true;
    }
    return false;
}
