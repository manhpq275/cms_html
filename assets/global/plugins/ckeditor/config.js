/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.baseFloatZIndex = 100001;
	config.filebrowserImageBrowseUrl = 'http://localhost:8080/CKFinderJava/ckfinder/ckfinder.html?type=Images';
	config.filebrowserImageUploadUrl = 'http://localhost:8080/CKFinderJava/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		'/',
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'NewPage,Save,Print,Templates,Find,Replace,SelectAll,Scayt,Form,TextField,Textarea,Button,Select,Checkbox,Radio,ImageButton,HiddenField,CopyFormatting,RemoveFormat,BidiRtl,BidiLtr,Language,Anchor,Flash,HorizontalRule,Iframe,Maximize,About';
};