$("#category_message").hide();
var ProductRequest = function (){
    this.id = -1;
    this.maDviQly = bussinessCode;
    this.dmHangHoa = -1;
    this.searchData = localStorage.productSearch === undefined ? "" : removeCharacter(localStorage.productSearch);

    // init DM_HANG_HOA_CT : id, maDviQly, tenHang, dmHangHoa, ttrangHhoa, giaTri, giaNiemYet, vat, tthaiBghi, ttrangNvu, anhDdien.
    // init DM_HHOA_TTINH : id, maDviQly, maTtinh, tenTtinh
    // init DM_HHOA_TTINH_CT: id, idTtinh, maTtinh, gtri
    // init DM_HHOA_CT_TTINH: id, maDviQly, idHHCT, idHHTTCT
    // init DM_HHOA_CT_TTINH_BSUNG: id, maDviQly, dmHangHoa, dmHangHoaCT, tenHang, moTa, sale, sanPhamMoi, quaTang, noiBat, hienthi, listImage
    this.tenHang = "";
    this.giaTri = 0;
    this.giaNiemYet = 0;
    this.sale = 0;
    this.vat = 0;
    this.moTa = 0;
    this.chiTiet = "";
    this.sanPhamMoi = 0;
    this.quaTang = 0;
    this.noiBat = 0;
    this.hienThi = 0;
    this.listTT = new Array();
    
    this.tthaiBghi = "KSD";
    this.ttrangNvu = "CHD";
    this.ttrangHhoa = 101;
    this.anhDdien = "";

    this.listAnh = new Array();
    this.collection = -1;
    this.group = -1;
    
    this.tags = '0';

    this.page = -1;
    this.limit = -1;
    this.toJson = function (){
        return JSON.stringify(this);
    };
};

var TrangThaiBanGhi = function (input){
    this.input = input;
    this.toString = function (){
        switch(this.input) {
            case "SDU":
                return "Đang sử dụng";
            default:
                return "Không sử dụng";
        }
    };
};

var TrangThaiNghiepVu = function (input){
    this.input = input;
    this.toString = function (){
        switch(this.input) {
            case "DDU":
                return "Đã duyệt";
            default:
                return "Chờ duyệt";
        }
    };
};

var totalPage = 0;
var page = 1;
var limitPage = 10;
var dataPage = new Array();
var dataEdit;
var type;
var productRequest = new ProductRequest();
var productRespone;
var rootProductRespone = null;
var listValueRespone;
var allProperties = null;
var allValueProperties = null;
var searchData = "";
var listProperties  = new Array();
var groupSelect = -1;
var Node = function (item){
  this.text = item.ten;
  this.data = item;
  this.children = new Array();
  this.children_d = new Array();
}


$(document).ready(function() {
  page = $(location).attr('href').split("#")[2];
 
  setTimeout(function(){
   initToTreeNode();
    requestListDM(0,-1);
 }, 100);

  setTimeout(function(){
    if (page === undefined) {
      page = 1;
      $("#productSearch").val("");
      groupSelect = -1;
      requestList(parseInt(page),parseInt(limitPage));
    } else {
      groupSelect = localStorage.groupSelect === undefined ? -1 : localStorage.groupSelect;
      $("#productSearch").val(localStorage.productSearch === undefined ? "" : localStorage.productSearch);
      requestList(parseInt(page),parseInt(limitPage));
    }

    requestRootProductList(null);
  },100);

  //request all properties
  requestAllProperties(0);
 
});

function requestAllProperties(count){
  if (allProperties == null) {
    requestService(urlListProperty,JSON.stringify({maDviQly: bussinessCode}), function(data){
      if (data != false) {
        if (data.totalResult > 0) {
          allProperties = data.list;
        } else {
          allProperties = null;
        }
      } else {
        if (count < 3) {
          count++;
          requestAllProperties(count);
        }
      }
    });
  }
}

function initProperties(divProperties, divValues, listDetail){
  //listNameProperty
  var allProperties = new Array();
  allValueProperties = listDetail;

  listDetail.forEach(function(item, i){

    if (allProperties.length === 0) {
      allProperties.push(item.idTtinh);
    } else {

      var check = false;

      allProperties.forEach(function(property, index){
        if (property.id === item.idTtinh.id) {
          check = true;
        }
      });

      if (!check) {
        allProperties.push(item.idTtinh);
      }
    }
  });

  $(divProperties).html("");
  $(divProperties).append('<option value="0">Tên thuộc tính</option>');
  allProperties.forEach(function(property, index){
    $(divProperties).append(new Option(property.tenTtinh, property.id));
  });
  initValueProperties(divValues, 0);

}



function initValueProperties(divValues, propertyId){
  $(divValues).html("");
  $(divValues).append('<option value="0">Giá trị thuộc tính</option>');
  allValueProperties.forEach(function(item, i){
    if (propertyId === item.idTtinh.id) {
      $(divValues).append(new Option(item.giaTri, item.id));
    }
  });
}

function requestList(pageNumber, limit){
  localStorage.productSearch = $("#productSearch").val();
  console.log ("start: " + new Date().getTime());
  productRequest = new ProductRequest();
  productRequest.page = pageNumber;
  productRequest.limit = limit;
  productRequest.group = groupSelect;
  $.ajax({
    url: urlProductDetail,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: productRequest.toJson()})
    .done(function (response, textStatus){
       console.log ("end: " + new Date().getTime());
          productRespone =  ResponeObject.fromRespone(response);
          console.log(response);
          $("#pageNumber").html("");
          $("#tableContent").html("");
          if (response.totalResult === undefined || response.totalResult === 0) {
            $("#loading_view").hide();
          } else {
            dataPage = productRespone.list;
            $("#totalResult").html("Hiển thị "+ response.list.length +" trên tổng số " + response.totalResult + " danh mục");
            if (parseInt(response.totalResult) <= parseInt(limit)) {
              $("#contentPageX").hide();
            } else {
              $("#contentPageX").show();
              totalPage = parseInt(response.totalResult / limit);
              if (response.totalResult % limit !== 0) {
                totalPage = totalPage + 1
              }
               var backPage = pageNumber - 1;
               var nextPage = pageNumber + 1;
               if (pageNumber >= totalPage) {
                  nextPage = totalPage;
               }
               if (pageNumber <= 1) {
                  backPage = 1;
               }
             
              $("#pageNumber").append('<li class="prev"><a href="#productList#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
              $("#pageNumber").append('<li class="prev"><a href="#productList#'+ backPage +'" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');
              
              if (totalPage > 6) {

                if(page != 1){
                  $("#pageNumber").append('<li><a href="#productList#'+(parseInt(page) - 1)+'">...</a></li>');
                } 

                if (page >= totalPage - 3) {
                    for (var i = (totalPage - 3); i <= totalPage; i++) {
                      $("#pageNumber").append('<li><a href="#productList#'+i+'">'+ i +'</a></li>');
                    }
                } else {
                    for (var i = page; i <= (parseInt(page) + 3); i++) {
                      $("#pageNumber").append('<li><a href="#productList#'+i+'">'+ i +'</a></li>');
                    }
                    $("#pageNumber").append('<li><a href="#productList#'+(parseInt(page) + 1)+'">...</a></li>');
                }
              } else {
                for (var i = 1; i <= totalPage; i++) {
                  $("#pageNumber").append('<li><a href="#productList#'+i+'">'+ i +'</a></li>');
                }
              }

              $("#pageNumber").append('<li class="next"><a href="#productList#'+ nextPage +'" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
              $("#pageNumber").append('<li class="next"><a href="#productList#'+ totalPage +'" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
            }
             
             $.each(productRespone.list, function(i, item) {
                var stt = (page - 1 )* limit + i + 1;
                var tmp =  '<tr class="odd gradeX">';
                    tmp += '<td>'+ stt +'</td>';
                    tmp += '<td>'+ item.dmHhoaCtTtinhBsung.tenBsung +'</td>';
                    tmp += '<td>'+ item.dmHhoaCtTtinhBsung.dmHangHoaCt.maVach +'</td>';
                    tmp += '<td>'+ item.dmHhoaCtTtinhBsung.dmHangHoaCt.ttrangHhoa.tenDmuc +'</td>';
                    tmp += '<td> Giá: '+ moneyFormat(item.dmHhoaCtTtinhBsung.dmHangHoaCt.giaTri) + "<br/>Giá niêm yết: "+ moneyFormat(item.dmHhoaCtTtinhBsung.dmHangHoaCt.giaNiemYet) +'</td>';
                    if (item.dmHhoaCtTtinhBsung.dmHangHoaCt.anhHh === undefined || item.dmHhoaCtTtinhBsung.dmHangHoaCt.anhHh === null) {
                      tmp += '<td>Chưa có ảnh</td>';
                    } else {
                      tmp += '<td><a class="btn green" data-toggle="modal" href="#largeImg" onclick="viewImage('+ item.dmHhoaCtTtinhBsung.dmHangHoaCt.id +');" >Xem ảnh</a></td>';
                    }
                    
                    tmp += '<td><a class="btn purple edit" data-toggle="modal" href="#large" onclick="editProduct('+ item.dmHhoaCtTtinhBsung.id +');">Sửa  <i class="fa fa-edit"></i></a>';
                    tmp += '<button onclick="deleteProduct('+ item.dmHhoaCtTtinhBsung.id +');" class="btn gray delete" attr-data="'+ item.dmHhoaCtTtinhBsung.id +'">Xóa  <i class="fa fa-trash-o"></i></button></td></tr>';
                $("#tableContent").append(tmp);
            });
            $("#loading_view").hide()
          }
         
     }).fail(function (){
       console.log( "loi" );
       $("#loading_view").hide();
     });
}

function initRootParent(){
  if (rootProductRespone == null) {
      requestRootProductList(function(){
      rootParent();
    });
  } else {
    rootParent();
  }
  
}

function rootParent(){
  $("#productRootId").hide();
  $("#detailProduct").hide();

  var rootProductList = Array();
  $("#productRootId").val("");
  $("#productRoot").val("");
  $("#listTrangThaiBG").html("");
  $("#listTrangThaiBG").append(new Option("Đang sử dụng","SDU"));
  $("#listTrangThaiBG").append(new Option("Không sử dụng","KSD"));
  $("#listTrangThaiNV").html("");
  $("#listTrangThaiNV").append(new Option("Chờ duyệt","CHD"));
  $("#listTrangThaiNV").append(new Option("Đã duyệt","DDU"));
  for (var i = 0; i < rootProductRespone.list.length; i++) {
     rootProductList.push(rootProductRespone.list[i].tenHang);         
  }

  $("#listValue").html("");
  $("#listValue").append(new Option("Chọn tình trạng hàng hóa",-1));
  for (var i = 0; i < listValueRespone.list.length; i++) {
    $("#listValue").append(new Option(listValueRespone.list[i].tenDmuc, listValueRespone.list[i].id));
  }
  autocomplete(document.getElementById("productRoot"), rootProductList, function(data){
      $("#productRootId").val(rootProductRespone.list[parseInt(data)].id);
      $("#productRoot").val(rootProductRespone.list[parseInt(data)].tenHang);
      onChangeProductRoot();
  });
  var data = JSON.parse(localStorage.allTags);
  $("#productTags").select2({
    tags: data
  });
      $("#productCode").val("");
      $("#productName").val("");
      $("#productGia").val("");
      $("#productGiaTri").val("");
      $("#productGiaNiemYet").val("");
      $("#productSale").val("");
      $("#productVat").val("");

      // $("#productDes").val("");

      $("#groupListCollection").show();
      $("#groupListGroup").show();
      $("#productImage").attr('src', '');
      $("#productImage1").attr('src', '');
      $("#productImage2").attr('src', '');
      $("#productImage3").attr('src', '');
      $("#productImage4").attr('src', '');
}

function requestRootProductList(callback){
  $.ajax({
    url: urlRootProduct,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({MA_DVI: bussinessCode})
        }).done(function (response, textStatus){
          console.log("requestRootProductList: ");
          console.log(response);
          rootProductRespone =  ResponeObject.fromRespone(response);
          requestListValue(callback);
     }).fail(function (){
       console.log( "loi requestRootProductList" );
     });
}

function requestListValue(callback){
  productRequest = new ProductRequest();
  productRequest.maDviQly = bussinessCode;
  $.ajax({
    url: urlListValue,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: productRequest.toJson()
        }).done(function (response, textStatus){
          console.log("requestListValue: ");
          console.log(response);
           listValueRespone =  ResponeObject.fromRespone(response);
           requestGroupProduct();
           requestCollection();
           if (callback != null) {
              callback();
           }
     }).fail(function (){
       console.log( "loi requestListValue" );
     });
}

function requestGroupProduct(){
  requestService(urlListGroupProduct,JSON.stringify({MA_DVI:bussinessCode}),function(response){
    console.log("requestGroupProduct: ");
    if (response != false) {
        $("#listGroup").html("");
        $("#listGroup").append(new Option("Chọn nhóm hàng hóa", -1));
        for (var i = 0; i < response.list.length; i++) {
          $("#listGroup").append(new Option(response.list[i].ten, response.list[i].id));
        }
    }
  });
}

function requestCollection(){
  requestService(urlListCollection,JSON.stringify({MA_DVI:bussinessCode}),function(response){
    console.log("requestCollection: ");
    if (response != false) {
        $("#listCollection").html("");
        $("#listCollection").append(new Option("Chọn bộ sưu tập", -1));
        for (var i = 0; i < response.list.length; i++) {
          $("#listCollection").append(new Option(response.list[i].tieuDe, response.list[i].id));
        }
    }
  });
}

function deleteProduct(e){
  if (confirm('Bạn có chắc chắn muốn xóa sản phẩm?')) {
    $.ajax({
    url: urlDeleteProduct,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({productId: e})
        }).done(function (response, textStatus){
          console.log(response)
           if (response === "true" || response == true) {
              alert("Xóa thành công");
              requestList(parseInt(page),parseInt(limitPage));
           } else {
              alert("Xóa sản phẩm không thành công");
           }
         
     }).fail(function (){
       console.log( "loi" );
       $("#loading_view").hide();
       alert("Xóa sản phẩm không thành công");
     });
  }
    
}


function clickAddNew(){
  dataEdit = null;
  type = "";
  initRootParent();
  seoTMP = new SeoRequest();
  $("#dataEditName").val("");
  $("#dataEditURL").val("");
  CKEDITOR.instances.productDes.setData("");
  CKEDITOR.instances.productDesDetail.setData("");
  $("#submit").html("Thêm mới");
  $(".modal-title").html("Thêm mới Sản phẩm");
  $("#helperSEO").html("Chưa có cấu hình SEO");
  $("#helperSEO").show();
  $("#productMessage").html('(*) Phần không được để trống');
  $("#productProperties").html("");
  listProperties = new Array();
}

function seoEditor(){
  $(".modal-title-seo").html("Cấu hình SEO sản phẩm");
  $("#seoMessage").html("(*) Phần bắt buộc phải nhập");
  $("#seoType").val("4");
  $("#helpSlug").html("");
  if (seoTMP.id !== -1) {
    $("#seoTitle").val(seoTMP.title);
    $("#seoSlug").val(seoTMP.slug);
    $("#seoDescription").val(seoTMP.description);
    $("#seoImage").attr("src", seoTMP.ogImage);

  } else {
    $("#seoTitle").val($("#productName").val());
    $("#seoSlug").val('hh-' + removeCharacter($("#productName").val()).toLowerCase().replaceAll(' ','-'));
    $("#seoDescription").val("");
    $("#seoImage").attr("src", $("#productImage").attr('src'));
  }

  $("#seoKeyWorks").val(seoTMP.keywords);
  $("#seoFavicon").attr("src", seoTMP.favico);
  $("#seoOGWIDTH").val(seoTMP.ogImageWidth);
  $("#seoOGHEIGHT").val(seoTMP.ogImageHeight);
  $("#seoOGSITE").val(seoTMP.seoOGSITE);
  // $("#seoOGTITLE").val(seoTMP.title);
 
  $("#seoOGTYPE").val(seoTMP.ogType);
  // $("#seoOGURL").val(seoTMP.ogUrl);
  $("#seoOGLOCALE").val(seoTMP.ogLocale);
  $("#seoArticleTAG").val(seoTMP.articleTag);
  // $("#seoTwitterCard").val(seoTMP.twitterCard);
  // $("#seoTwitterDescription").val(seoTMP.description);
  // $("#seoTwitterTitle").val(seoTMP.title);
}

function getProductPrice(dataRequest){
   $.ajax({
      url: urlGetPriceByHHCT,
      type: 'post',
      dataType: 'json',
      contentType: 'application/json;charset=UTF-8',
      data: dataRequest
      }).done(function (response, textStatus){
        if(typeof response == 'number') {
          $("#productGia").val(moneyFormat(response));
         }    
      }).fail(function (){
         console.log( "Error Request: "+ urlRequest);
         callBack(false);
       });
}

function editProduct(e){
    initRootParent();
    $("#productProperties").html("");
    listProperties = new Array();
   $.each(dataPage, function(i, item) {
    if (item.dmHhoaCtTtinhBsung.id === e) {
      
      $("#productMessage").html('(*) Phần không được để trống');
      $("#detailProduct").show();
      $("#submit").html("Sửa");
      console.log(item.dmHhoaCtTtinhBsung);
      dataRequest = new ProductRequest();
      dataRequest.id = item.dmHhoaCtTtinhBsung.dmHangHoaCt.id;
      type = "edit";
  
      var dataListProperty = {maDviQly: bussinessCode, idHHCT:item.dmHhoaCtTtinhBsung.dmHangHoaCt.id};
      listProperties = new Array();

      getProductPrice(JSON.stringify({idProduct : item.dmHhoaCtTtinhBsung.dmHangHoaCt.id}));

      requestService(urlGetListProperty,JSON.stringify(dataListProperty),function(response){
        console.log(response);
        requestRouteSeo("4", item.dmHhoaCtTtinhBsung.dmHangHoaCt.id, function(isResult){
              if (!isResult) {
                  $("#helperSEO").html("Chưa có cấu hình SEO");
                  $("#helperSEO").show();
                  seoTMP = new SeoRequest();
              } else {
                  seoTMP = seoObject;
                  $("#helperSEO").hide();
              }
        });
        if (response.totalResult >0) {
          response.list.forEach(function(item){
              var idListProperty = listProperties.length;
              addNewProperty();
              setTimeout(function(){
                $("#productProperty"+ idListProperty +" option[value="+ item.idTtinhCt.idTtinh.id +"]").attr("selected","selected");
                initValueProperties("#productPropertyValue"+ idListProperty, parseInt($("#productProperty"+ idListProperty).val()));
                setTimeout(function(){
                  $("#productPropertyValue"+ idListProperty +" option[value="+ item.idTtinhCt.id +"]").attr("selected","selected");
                },100);
              },100);
          });

          
            //$("#listNameProperty option[value="+ response.list[0].idTtinhCt.idTtinh.id +"]").attr("selected","selected");
           // initValueProperties(response.list[0].idTtinhCt.idTtinh.id);
            //$("#listValueProperty option[value="+ response.list[0].idTtinhCt.id +"]").attr("selected","selected");
        }
      });
      $("#productRootId").val(item.dmHhoaCtTtinhBsung.dmHangHoa.id);
      $("#productRoot").val(item.dmHhoaCtTtinhBsung.dmHangHoa.tenHang);
      $("#productCode").val(item.dmHhoaCtTtinhBsung.dmHangHoaCt.maVach);
      $("#productName").val(item.dmHhoaCtTtinhBsung.tenBsung);
      $("#productGiaTri").val(moneyFormat(item.dmHhoaCtTtinhBsung.dmHangHoaCt.giaTri));
      $("#productGiaNiemYet").val(moneyFormat(item.dmHhoaCtTtinhBsung.dmHangHoaCt.giaNiemYet));
      $("#productSale").val(moneyFormat(item.dmHhoaCtTtinhBsung.sale));
      $("#productVat").val(moneyFormat(item.dmHhoaCtTtinhBsung.dmHangHoaCt.vat));
      CKEDITOR.instances.productDesDetail.setData(item.dmHhoaCtTtinhBsung.chiTiet);
      CKEDITOR.instances.productDes.setData(item.dmHhoaCtTtinhBsung.moTa);
      $('#productNew').prop('checked',(item.dmHhoaCtTtinhBsung.spMoi === 1));
      $('#productGift').prop('checked',(item.dmHhoaCtTtinhBsung.quaTang === 1));
      $('#productSpecial').prop('checked',(item.dmHhoaCtTtinhBsung.noiBat === 1));
      $('#productShow').prop('checked',(item.dmHhoaCtTtinhBsung.hienThi === 1));

      $("#groupListCollection").hide();
      $("#groupListGroup").hide();

      $("#listTrangThaiBG option[value="+item.dmHhoaCtTtinhBsung.dmHangHoaCt.tthaiBghi+"]").attr("selected","selected");
      $("#listTrangThaiNV option[value="+item.dmHhoaCtTtinhBsung.dmHangHoaCt.tthaiNvu+"]").attr("selected","selected");
      $("#listValue option[value="+item.dmHhoaCtTtinhBsung.dmHangHoaCt.ttrangHhoa.id+"]").attr("selected","selected");
      
      $("#productImage").attr('src', item.dmHhoaCtTtinhBsung.dmHangHoaCt.anhHh);
      $("#productImage1").attr('src', item.dmHhoaCtTtinhBsung.hinhAnh);
      $("#productImage2").attr('src', item.dmHhoaCtTtinhBsung.hinhAnh2);
      $("#productImage3").attr('src', item.dmHhoaCtTtinhBsung.hinhAnh3);
      $("#productImage4").attr('src', item.dmHhoaCtTtinhBsung.hinhAnh4);

      var tmpTag = new Array();
      $.each(item.dsTAGS, function (i, tag) {
        tmpTag.push(tag.keyword);
      });
      $("#productTags").select2('val',tmpTag);
    }
  });
}

function viewImage(e){
  $.each(dataPage, function(i, item) {
    if (item.dmHhoaCtTtinhBsung.dmHangHoaCt.id === e) {
      console.log(item);
      $("#imgSelected").attr("src", item.dmHhoaCtTtinhBsung.dmHangHoaCt.anhHh);
    }
  });
}

function deleteAll(){
  console.log("deleteAll");
}

$("#submit").click(function(){
    if (type != "edit") {
      dataRequest = new ProductRequest();
    }
    if (seoTMP.title === "") {
      $("#productMessage").html("Chưa có cấu hình SEO");
    } else {
        dataRequest.dmHangHoa = parseInt($("#productRootId").val());
        dataRequest.tenHang = $("#productName").val();
        dataRequest.giaTri = $("#productGiaTri").val().replaceAll(".","");
        dataRequest.giaNiemYet = $("#productGiaNiemYet").val().replaceAll(".","");
        dataRequest.sale = $("#productSale").val().replaceAll(".","");
        dataRequest.vat = $("#productVat").val().replaceAll(".","");
        dataRequest.moTa = CKEDITOR.instances.productDes.getData();
        dataRequest.chiTiet =  CKEDITOR.instances.productDesDetail.getData();
        dataRequest.sanPhamMoi = $('#productNew').is(":checked") ? 1 : 0;
        dataRequest.quaTang = $('#productGift').is(":checked") ? 1 : 0;
        dataRequest.noiBat = $('#productSpecial').is(":checked") ? 1 : 0;
        dataRequest.hienThi = $('#productShow').is(":checked") ? 1 : 0;
        dataRequest.collection = $("#listCollection").val();
        dataRequest.group = $("#listGroup").val();
        
        dataRequest.listTT = new Array();
        for (var i = 0; i < listProperties.length; i++) {
          if (listProperties[i] != -1) {
            dataRequest.listTT.push($("#productPropertyValue"+listProperties[i]).val());
          }
        }

        console.log(dataRequest.listTT);
        dataRequest.tthaiBghi = $("#listTrangThaiBG").val();
        dataRequest.ttrangNvu = $("#listTrangThaiNV").val();
        dataRequest.ttrangHhoa = $("#listValue").val();
        var imageHH = new ImageCMS();
        imageHH.url = $("#productImage").attr('src');
        imageHH.alt = $("#productImageAlt").val();
        dataRequest.anhDdien = imageHH.toString();
        dataRequest.listAnh = new Array();
        dataRequest.tags = $("#productTags").val();
        for (var i = 1; i <= 4; i++) {
          var tmp = new ImageCMS();
          tmp.url = $("#productImage"+i).attr('src');
          tmp.alt = $("#productImageAlt"+i).val();
          dataRequest.listAnh.push(tmp.toString());
        }
        console.log(dataRequest);
        $.ajax({
        url: urlCreateProduct,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: dataRequest.toJson()
            }).done(function (response, textStatus){
              if (response.errorCode === null || response.errorCode === undefined || response.errorCode === "undefined") {
                    console.log( "create Hang Hoa: " );
                    console.log( response );
                    $("#closePopup").click();
                    if (type=== "edit") {
                      alert("Sửa thành công");
                    } else {
                      alert("Thêm mới thành công");
                    }
                   setTimeout(function(){
                    seoTMP.idLienKet = response.list[0].dmHangHoaCt.id;
                      createRouteSeo(seoTMP, function(isResult){
                            seoObject = new SeoRequest();
                            requestList(parseInt(page),parseInt(limitPage));
                      });
                    },200);
               } else {
                  $("#productMessage").html(response.msg);
                  if (type=== "edit") {
                    alert("Sửa không thành công");
                  } else {
                    alert("Thêm mới không thành công");
                  }
               }
         }).fail(function (){
            console.log( "loi" );
            if (type=== "edit") {
                alert("Sửa không thành công");
            } else {
                alert("Thêm mới không thành công");
            }
         });
    }
});

$("#closePopup").on('click',function(){
    seoObject = new SeoRequest();
});

$("#closePopup2").on('click',function(){
    seoObject = new SeoRequest();
});


function onChangeProductRoot(){
  for (var i = 0; i < rootProductRespone.list.length; i++) {
    if (rootProductRespone.list[i].id == parseInt($("#productRootId").val())) {
       $("#productName").val(rootProductRespone.list[i].tenHang);
       $("#avatarProduct").attr("src",rootProductRespone.list[i].anhHh);
    }
  }

  $("#detailProduct").show();
}


$("#productGiaTri").on('change input keyup paste', function(){
  var tmp = $("#productGiaTri").val().replaceAll(".","");
  $("#productGiaTri").val(moneyFormat(tmp));
});
$("#productGiaNiemYet").on('change input keyup paste', function(){
  var tmp = $("#productGiaNiemYet").val().replaceAll(".","");
  $("#productGiaNiemYet").val(moneyFormat(tmp));
});
$("#productSale").on('change input keyup paste', function(){
  var tmp = $("#productSale").val().replaceAll(".","");
  $("#productSale").val(moneyFormat(tmp));
});
$("#productVat").on('change input keyup paste', function(){
  var tmp = $("#productVat").val().replaceAll(".","");
  $("#productVat").val(moneyFormat(tmp));
});

function addNewProperty(){
  var idListProperty = listProperties.length;
  var tmp =  '<div class="form-group" id="productPropertyIndex'+ idListProperty +'">';
      tmp += '<label class="col-md-4 control-label"></label>';
      tmp += '<div class="col-md-3">';
      tmp += '<select class="form-control input-circle" id="productProperty'+ idListProperty +'"></select></div>';
      tmp += '<div class="col-md-3">';
      tmp += '<select class="form-control input-circle" id="productPropertyValue'+ idListProperty +'"></select></div>';
      tmp += '<div class="col-md-1"><a class="btn green circle" onclick="removeProperty('+ idListProperty +');">Xóa&nbsp;<i class="fa fa-trash-o"></i></a></div></div>';
  $("#productProperties").append(tmp);
  listProperties.push(idListProperty);
  $("#productProperty"+ idListProperty).change(function(){
    if ($("#productProperty"+ idListProperty).val() === 0) {
      $("#productPropertyValue"+ idListProperty).html('<option value="0">Giá trị thuộc tính</option>');
    } else {
      initValueProperties("#productPropertyValue"+ idListProperty, parseInt($("#productProperty"+ idListProperty).val()));
    }
  });
  initProperties("#productProperty"+ idListProperty, "#productPropertyValue"+ idListProperty, allProperties);
}

function removeProperty(index){
  var idRemove = "#productPropertyIndex"+listProperties[index];
  console.log(idRemove); 
  $(idRemove).remove();
  listProperties[index] = -1;
  console.log(listProperties); 
}

function clickSearch() {
    page = 1;
    requestList(parseInt(page), parseInt(limitPage));
}

$('#productSearch').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          clickSearch();
        }
    });
// <div class="form-group">
//                               <label class="col-md-4 control-label">Thuộc tính sản phẩm <span style="color: red">(*)</span>:</label>
//                               <div class="col-md-4">
//                                 <select class="form-control input-circle" id="listNameProperty">
//                                   <option value="0">Tên thuộc tính</option>
//                                 </select>
//                               </div>
//                               <div class="col-md-4">
//                                 <select class="form-control input-circle" id="listValueProperty">
//                                   <option value="0">Giá trị thuộc tính</option>
//                                 </select>
//                               </div>
//                             </div>



function initToTreeNode(){
 $("#treeGroup").jstree({
  "core" : {
    "themes" : {
      "responsive": true
    }, 
                // so that create works
                "check_callback" : true,
                'data' : listNode
              },
              "types" : {
                "default" : {
                  "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                  "icon" : "fa fa-file icon-state-warning icon-lg"
                }
              },
              "plugins" : ["types" ]
            });
}

var listNode = new Array();
function requestListDM(pageNumber, limit){
  $.ajax({
    url: httpBaseRequest + "listGroupProduct",
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({MA_DVI:bussinessCode, page: pageNumber, limit: limit})
  }).done(function (response, textStatus){
   
   if (response.totalResult == 0) {
    $("#loading_view").hide();
  } else {
    listNode = new Array();
    var nodeAll = new Node({ten: "Tất cả sản phẩm", id: -1});
    listNode.push(nodeAll);
    response.list.forEach(function(item) {
      if (item.idCha === undefined) {
        var isAlive = false;
        listNode.forEach(function(node){
          if (node.data.id == item.id) {
            isAlive = true;
          }
        });
        if (!isAlive) {
          listNode.push(new Node(item));
        }
      } else {
        if (listNode.length == 0) {
          var parentNode = new Node(item.idCha);
          parentNode.children.push(new Node(item));
          listNode.push(parentNode);
        } else {
          var isAlive = false;
          listNode.forEach(function(node){
            if (node.data.id == item.idCha.id) {
              isAlive = true;
              node.children.push(new Node(item));
            }
          });
          if (!isAlive) {
            var parentNode = new Node(item.idCha);
            parentNode.children.push(new Node(item));
            listNode.push(parentNode);
          }
        }
      }
    });
    setTimeout(function(){
      $('#treeGroup').jstree(true).settings.core.data = listNode;
      $('#treeGroup').jstree(true).refresh();
    },200);
    
  }

}).fail(function (){
 console.log( "loi" );
});
}

$('#treeGroup').on('select_node.jstree', function(e,data) { 
  groupSelect = data.node.data.id;
  localStorage.groupSelect = data.node.data.id;
  requestList(parseInt(page),parseInt(limitPage));
});
