var ResponeObject = function (totalResult, list){
	
    this.totalResult = totalResult;
    this.list = list;
    this.toJson = function (){
        return JSON.stringify(this);
    };
};

ResponeObject.fromJson = function (json){
    var obj = JSON.parse (json);
    return new ResponeObject (obj.totalResult, obj.list);
};

ResponeObject.fromRespone = function (data){
    return new ResponeObject (data.totalResult, data.list);
};
