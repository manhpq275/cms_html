var urlListCustomers = httpBaseRequest + "getListCustomers";

var CustomersRequest = function () {

    this.page = -1;
    this.limit = -1;
    this.searchData = "";

    this.toJson = function () {
        return JSON.stringify(this);
    };
}

var totalPage = 0;
var page = $(location).attr('href').split("#")[2];
var limitPage = 50;
var dataPage = new Array();
var productRequest = new CustomersRequest();
var productRespone;

if (page === undefined) {
    page = 1;
    requestList(parseInt(page), parseInt(limitPage), $("#customerRoot").val());
} else {
    requestList(parseInt(page), parseInt(limitPage), $("#customerRoot").val());
}

function requestList(pageNumber, limit, search) {
    productRequest = new CustomersRequest();
    productRequest.page = pageNumber;
    productRequest.limit = limit;
    productRequest.searchData = search;

    $.ajax({
        url: urlListCustomers,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: productRequest.toJson({page: pageNumber, limit: limit, searchData: search})})
            .done(function (response, textStatus) {
                productRespone = ResponeObject.fromRespone(response);
                console.log(response);

                $("#pageNumber").html("");
                $("#tableContent").html("");

                if (response.totalResult == 0) {
                    $("#loading_view").hide();
                } else {
                    dataPage = productRespone.list;

                    $("#totalResult").html("Hiển thị " + response.list.length + " trên tổng số " + response.totalResult + " khách hàng");

                    if (parseInt(response.totalResult) <= parseInt(limit)) {
                        $("#contentPageX").hide();
                    } else {
                        totalPage = parseInt(response.totalResult / limit);
                        if (response.totalResult % limit !== 0) {
                            totalPage = totalPage + 1
                        }
                        var backPage = pageNumber - 1;
                        var nextPage = pageNumber + 1;
                        if (pageNumber >= totalPage) {
                            nextPage = totalPage;
                        }
                        if (pageNumber <= 1) {
                            backPage = 1;
                        }

                        $("#pageNumber").append('<li class="prev"><a href="#listCustomers#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
                        $("#pageNumber").append('<li class="prev"><a href="#listCustomers#' + backPage + '" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');

                        if (totalPage > 6) {

                            if (page != 1) {
                                $("#pageNumber").append('<li><a href="#listCustomers#' + (parseInt(page) - 1) + '">...</a></li>');
                            }

                            if (page >= totalPage - 3) {
                                for (var i = (totalPage - 3); i <= totalPage; i++) {
                                    $("#pageNumber").append('<li><a href="#listCustomers#' + i + '">' + i + '</a></li>');
                                }
                            } else {
                                for (var i = page; i <= (parseInt(page) + 3); i++) {
                                    $("#pageNumber").append('<li><a href="#listCustomers#' + i + '">' + i + '</a></li>');
                                }
                                $("#pageNumber").append('<li><a href="#listCustomers#' + (parseInt(page) + 1) + '">...</a></li>');
                            }
                        } else {
                            for (var i = 1; i <= totalPage; i++) {
                                $("#pageNumber").append('<li><a href="#listCustomers#' + i + '">' + i + '</a></li>');
                            }
                        }

                        $("#pageNumber").append('<li class="next"><a href="#listCustomers#' + nextPage + '" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
                        $("#pageNumber").append('<li class="next"><a href="#listCustomers#' + totalPage + '" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
                    }

                    $.each(productRespone.list, function (i, item) {
						var ii = i+1;
                        var tmp = '<tr class="odd gradeX"><td>' + ii + '</td>';
                        tmp += '<td>' + item.maKhang + '</td>';
						tmp += '<td>' + item.maThchieu + '</td>';
                        tmp += '<td>' + item.tenKhang + '</td>';
                        tmp += '<td>' + item.idTheKhachhang + '</td>';
                        tmp += '<td>' + item.soDthoai + '</td>';
                        //tmp += '<td>Trạng thái bản ghi :' + TrangThaiBanGhi(item.tthaiBghi);
                        //tmp += '<br/> Trạng thái nghiệp vụ :' + TrangThaiNghiepVu(item.tthaiNvu) + '</td>';
                        tmp += '<td>' + item.maDviQly + '</td>';
                        tmp += '<td align="right">' + numberWithCommas(item.doanhSo) + '</td>';
                        //tmp += '<td><a class="btn purple edit" data-toggle="modal" href="#large" onclick="editCustomer(' + item.id + ');">Sửa  <i class="fa fa-edit"></i></a></td>';
                        //tmp += '<td><button onclick="deleteCustomer(' + item.id + ');" class="btn gray delete" attr-data="' + item.id + '">Xóa  <i class="fa fa-trash-o"></i></button></td></tr>';

                        tmp += '</tr>';

                        $("#tableContent").append(tmp);
                    });
                    $("#loading_view").hide()
                }
            }).fail(function () {
        console.log("loi");
        $("#loading_view").hide();
    });
}

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

var TrangThaiBanGhi = function (input) {
    this.input = input;
    this.toString = function () {
        switch (this.input) {
            case "SDU":
                return "Đang sử dụng";
            default:
                return "Không sử dụng";
        }
    };
};

var TrangThaiNghiepVu = function (input) {
    this.input = input;
    this.toString = function () {
        switch (this.input) {
            case "DDU":
                return "Đã duyệt";
            default:
                return "Chờ duyệt";
        }
    };
};

function clickSearch() {
    //alert($("#customerRoot").val());
    requestList(parseInt(page), parseInt(limitPage), $("#customerRoot").val());
}

$("#customerRoot").keypress(function (event) {
    if (event.which == 13) {
        //code here
        //13 represents Enter key
        requestList(parseInt(page), parseInt(limitPage), $("#customerRoot").val());
    }
});