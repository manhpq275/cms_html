
var dataEdit;
var type;

var listParent = new Array();
var parentSelect = 0;
var NewsRequest = function () {
    this.page = -1;
    this.limit = -1;
    this.searchData = "";
    this.maDviQly = bussinessCode;
    this.danhmuc = '0';
    this.tag = '0';
    this.id = 0;
    this.toJson = function () {
        return JSON.stringify(this);
    };
};

var NewsCreateRequest = function () {
    this.ten = -1;
    this.tags = '';
    this.tomTat = "";
    this.maDviQly = bussinessCode;
    this.idDanhMuc = '0';
    this.noiDung = '';
    this.hinhAnh = '';
    this.id = 0;
    this.toJson = function () {
        return JSON.stringify(this);
    };
};

var totalPage = 0;
var page = 1;
var limitPage = 10;
var dataPage = new Array();
var productRequest = new NewsRequest();
var productRespone;

$(document).ready(function() {
    page = $(location).attr('href').split("#")[2];
    if (page === undefined) {
        page = 1;
        requestList(parseInt(page), parseInt(limitPage), $("#newsRoot").val(), 0, 0);
    } else {
        requestList(parseInt(page), parseInt(limitPage), $("#newsRoot").val(), 0, 0);
    }

    requestListParent();

    var data = JSON.parse(localStorage.allTags);
    $("#newsTags").select2({
        tags: data
    });
    
});

function clickSearch() {
    //alert($("#customerRoot").val());
    requestList(parseInt(page), parseInt(limitPage), $("#newsRoot").val(),0, 0);
}

$("#newsRoot").keypress(function (event) {
    if (event.which == 13) {
        //code here
        //13 represents Enter key
        requestList(parseInt(page), parseInt(limitPage), $("#newsRoot").val(), 0, 0);
    }
});


$('#listInputParent').on('change', function(){
    if ($("#listInputParent option:selected").val() == 0) {
        $('.form-body').hide();
        if (dataEdit === undefined || dataEdit === null) {
            $(".modal-title").html("Thêm mới tin tức");
        }
    } else {
        $('.form-body').show();
        if (dataEdit === undefined || dataEdit === null) {
            $(".modal-title").html("Thêm mới tin tức vào danh mục: " + $("#listInputParent option:selected").text());
        }
    }
    
});

function requestList(pageNumber, limit, search, danhmuc, tag) {
    productRequest = new NewsRequest();
    productRequest.page = pageNumber;
    productRequest.limit = limit;
    productRequest.searchData = search;
    productRequest.danhmuc = localStorage.parentSelect === undefined ? 0 : localStorage.parentSelect;
    productRequest.maDviQly = bussinessCode;
    productRequest.tag = tag;

    $.ajax({
        url: urlListNews,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: productRequest.toJson({page: pageNumber, limit: limit, searchData: search, danhmuc: danhmuc, maDviQly: bussinessCode, tag: tag})})
            .done(function (response, textStatus) {
                productRespone = ResponeObject.fromRespone(response);
                console.log(response);

                $("#pageNumber").html("");
                $("#tableContent").html("");

                if (response.totalResult == 0) {
                    $("#loading_view").hide();
                } else {
                    dataPage = productRespone.list;

                    $("#totalResult").html("Hiển thị " + response.list.length + " trên tổng số " + response.totalResult + " tin");

                 
                        $("#contentPageX").show();
                        totalPage = parseInt(response.totalResult / limit);
                        if (response.totalResult % limit !== 0) {
                            totalPage = totalPage + 1
                        }
                        var backPage = pageNumber - 1;
                        var nextPage = pageNumber + 1;
                        if (pageNumber >= totalPage) {
                            nextPage = totalPage;
                        }
                        if (pageNumber <= 1) {
                            backPage = 1;
                        }

                        $("#pageNumber").append('<li class="prev"><a href="#newList#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
                        $("#pageNumber").append('<li class="prev"><a href="#newList#' + backPage + '" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');

                        if (totalPage > 6) {

                            if (page != 1) {
                                $("#pageNumber").append('<li><a href="#newList#' + (parseInt(page) - 1) + '">...</a></li>');
                            }

                            if (page >= totalPage - 3) {
                                for (var i = (totalPage - 3); i <= totalPage; i++) {
                                    $("#pageNumber").append('<li><a href="#newList#' + i + '">' + i + '</a></li>');
                                }
                            } else {
                                for (var i = page; i <= (parseInt(page) + 3); i++) {
                                    $("#pageNumber").append('<li><a href="#newList#' + i + '">' + i + '</a></li>');
                                }
                                $("#pageNumber").append('<li><a href="#newList#' + (parseInt(page) + 1) + '">...</a></li>');
                            }
                        } else {
                            for (var i = 1; i <= totalPage; i++) {
                                $("#pageNumber").append('<li><a href="#newList#' + i + '">' + i + '</a></li>');
                            }
                        }

                        $("#pageNumber").append('<li class="next"><a href="#newList#' + nextPage + '" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
                        $("#pageNumber").append('<li class="next"><a href="#newList#' + totalPage + '" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
                 

                    $.each(productRespone.list, function (i, item) {
                        var tmpTenDanhMuc = "";
                        listParent.forEach(function(danhMuc){
                            if (danhMuc.id === item.idDanhMuc) {
                                tmpTenDanhMuc = danhMuc.ten;
                            }
                        });
                        var tmp = '<tr class="odd gradeX"><td>' + ((page -1) * limit +  i + 1) + '</td><td>' + item.ten + '</td>';
                        tmp += '<td><a href="javascript:filterByCate(' + item.idDanhMuc + ');">' + tmpTenDanhMuc + '</a></td>';

                        //tmp += '<td>' + item.idTags + '</td>';

                        tmp += '<td>';
                        if (item.tags !== undefined && item.tags !== null){
                            $.each(item.tags, function (i, tag) {
                                tmp += '<a href="javascript:filterByTag(' + tag.id + ',' + item.idDanhMuc + ');" style="font-weight:bold;">' + tag.keyword + '</a>&nbsp;&nbsp;&nbsp;';
                            });
                        }
                        tmp += '</td>';

                        tmp += '<td>' + item.tomTat + '</td>';
                        tmp += '<td>' + (item.hinhAnh === undefined ? "" : '<img src="' + item.hinhAnh + '" width="120" height="120"/>') + '</td>';
                        tmp += '<td><a class="btn purple edit" data-toggle="modal" href="#createNews" onclick="editNew(' + item.id + ');">Sửa  <i class="fa fa-edit"></i></a>';
                        tmp += '<button onclick="deleteNew(' + item.id + ');" class="btn gray delete" attr-data="' + item.id + '">Xóa  <i class="fa fa-trash-o"></i></button></td></tr>';

                        $("#tableContent").append(tmp);
                    });
                    $("#loading_view").hide()
                }
            }).fail(function () {
        console.log("loi");
        $("#loading_view").hide();
    });
}

function editNew(id){
    $.each(dataPage, function(i, item) {
        if (item.id === id) {
            dataEdit = item;
            type = "edit";
            $(".form-body").show();
            $("#submit").html("Sửa");
            $("#categoryName").val(dataEdit.ten);
            $(".modal-title").html("Chỉnh sửa tin tức");
            $("#newsMessage").html("(*) : Bắt buộc phải nhập không được để trống");
            $("#listInputParent option[value='" + dataEdit.idDanhMuc + "']").prop('selected', true);
            $("#newsId").val(dataEdit.id);
            $("#newsTitle").val(dataEdit.ten);
            var tmpTag = new Array();
            if (item.tags !== undefined && item.tags !== null){
                $.each(item.tags, function (i, tag) {
                    tmpTag.push(tag.keyword);
                });
            }
            requestRouteSeo("5", dataEdit.id, function(isResult){
                if (!isResult) {
                  $("#helperSEO").html("Chưa có cấu hình SEO");
                  $("#helperSEO").show();
                } else {
                  $("#helperSEO").hide();
                }
              });

            $("#newsTags").select2('val',tmpTag);
            CKEDITOR.instances.newsSummary.setData(dataEdit.tomTat);
            CKEDITOR.instances.newsContent.setData(dataEdit.noiDung);
            $("#newsImage").attr('src', dataEdit.hinhAnh === undefined ? "" : dataEdit.hinhAnh);
            $("#createNews").toggle();
      }
  });
}

function clickAddNew(){
        $("#listInputParent option[value='0']").prop('selected', true);
        $(".form-body").hide();
        $("#createNews").modal();
        dataEdit = null;
        type = "";
        $("#submit").html("Thêm mới");
        $("#newsMessage").html("(*) : Bắt buộc phải nhập không được để trống");
        $(".modal-title").html("Thêm mới tin tức");
        $("#newsId").val("0");
        $("#newsTitle").val("");
        seoTMP = new SeoRequest();
        $("#newsTags").val("");
        CKEDITOR.instances.newsSummary.setData("");
        CKEDITOR.instances.newsContent.setData("");
        $("#newsImage").attr('src', '');
}


function deleteNew(id) {
    productRequest = new NewsRequest();
    productRequest.id = id;

    if (confirm('Bạn có chắc chắn muốn xóa tin tức?')) {
        $.ajax({
            url: urlDeleteNew,
            type: 'post',
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            data: productRequest.toJson()
        }).done(function (response, textStatus) {
            alert("Xóa thành công");
            requestList(parseInt(page), parseInt(limitPage), $("#newsRoot").val(), 0, 0);
        }).fail(function () {
            console.log("loi");
            alert("Xóa thất bại");
            $("#loading_view").hide();
        });
    }
}


function filterByTag(tag, cate) {
    requestList(parseInt(page), parseInt(limitPage), $("#newsRoot").val(), 0, tag);
}



function requestListParent() {
    $("#listInputParent").html("");
    $("#listInputParent").append(new Option("Chọn danh mục tin tức", 0));
    $.ajax({
        url: httpBaseRequest + "listNewsCategory",
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify({MA_DVI: bussinessCode})
    }).done(function (response, textStatus) {
        //$("#listParent").append(new Option("Chọn danh mục cha", 0));
        if (response.totalResult == 0) {
            $("#listInputParent").hide();
        } else {
            listParent = response.list;
            response.list.forEach(function(item, i){
                $("#listInputParent").append(new Option(response.list[i].ten, response.list[i].id));
            });
            var idSelected = localStorage.parentSelect === undefined ? 0 : localStorage.parentSelect;
        }

    }).fail(function () {
        console.log("loi");
    });

}

$("#submit").click(function(){
    if (isEmpty($("#newsTitle").val())) {
        $("#newsMessage").html("Tiêu đề bài viết không được bỏ trống.");
    } else if (isEmpty(CKEDITOR.instances.newsSummary.getData())) {
        $("#newsMessage").html("Mô tả bài viết không được bỏ trống.");
    } else if (isEmpty(CKEDITOR.instances.newsContent.getData())) {
        $("#newsMessage").html("Nội bài viết không được bỏ trống.");
    } else if (isEmpty($("#newsImage").attr('src'))) {
        $("#newsMessage").html("Ảnh đại diện không được để trống.");
    } else if (seoTMP.title === "") {
       $("#newsMessage").html("Chưa có cấu hình SEO");
    } else if ($("#listInputParent option:selected").val() === "0"){
        alert("Bạn phải chọn danh mục trước!");
    } else {

        var createRequest = new NewsCreateRequest();
        createRequest.id = $("#newsId").val();
        createRequest.ten = $("#newsTitle").val();
        createRequest.tags = $("#newsTags").val();
        createRequest.tomTat = CKEDITOR.instances.newsSummary.getData();
        createRequest.maDviQly = bussinessCode;
        createRequest.idDanhMuc = $("#listInputParent option:selected").val();
        
        createRequest.noiDung = CKEDITOR.instances.newsContent.getData();
        createRequest.hinhAnh = $("#newsImage").attr('src');
        console.log(createRequest.toJson());
        setTimeout(function(){
            requestService(urlCreateNews, createRequest.toJson(), function(data){
                if (data == false) {
                    $("#newsMessage").html("Không thể viết tin tức vào lúc này, vui lòng thử lại sau");
                } else {
                    setTimeout(function(){
                      seoTMP.idLienKet = data.list[0].id;
                      createRouteSeo(seoTMP, function(isResult){
                        seoObject = new SeoRequest();
                        alert("Thêm mới thành công");
                        requestList(parseInt(page), parseInt(limitPage), $("#newsRoot").val(),0, 0);
                        console.log(data);
                        $("#closePopup").click();
                      });
                    },200);
                }
            });
        },100);
        
    }
    
});

function seoEditor(){
    $(".modal-title-seo").html("Cấu hình SEO tin tức");
  $("#seoMessage").html("(*) Phần bắt buộc phải nhập");
  $("#seoType").val("5");
  $("#helpSlug").html("");
  if (seoTMP.id !== -1) {
    $("#seoTitle").val(seoTMP.title);
    $("#seoSlug").val(seoTMP.slug);
    $("#seoDescription").val(seoTMP.description);
    $("#seoImage").attr("src", seoTMP.ogImage);

  } else {
    $("#seoTitle").val($("#newsTitle").val());
    $("#seoSlug").val(removeCharacter('tt-' + $("#newsTitle").val()).toLowerCase().replaceAll(' ','-'));
    $("#seoDescription").val($("#newsSummary").val());
    $("#seoImage").attr("src", $("#newsImage").attr('src'));
  }

  $("#seoKeyWorks").val(seoTMP.keywords);
  $("#seoFavicon").attr("src", seoTMP.favico);
  $("#seoOGWIDTH").val(seoTMP.ogImageWidth);
  $("#seoOGHEIGHT").val(seoTMP.ogImageHeight);
  $("#seoOGSITE").val(seoTMP.seoOGSITE);
  $("#seoOGTYPE").val(seoTMP.ogType);
  $("#seoOGLOCALE").val(seoTMP.ogLocale);
  $("#seoArticleTAG").val(seoTMP.articleTag);
}