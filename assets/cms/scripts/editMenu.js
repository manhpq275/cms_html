$("#category_message").hide();
var MenuRequest = function (){
    this.id = null;
    this.idCha = null;
    this.loaiMenu = -1;
    this.tieuDe = null;
    this.url = null;
    this.hienThi = null;
    this.icon = null;
    this.background = null;
    this.maDviQly = bussinessCode;
    this.idLoaiMenu = -1;
    this.page = -1;
    this.limit = -1;
    this.toJson = function (){
        return JSON.stringify(this);
    };
};

var LoaiMenu = function (loaiMenu){
    this.loaiMenu = loaiMenu;
    this.toString = function (){
        switch(parseInt(this.loaiMenu)) {
            case 1:
                return "Nhóm hàng hóa";
                break;
            case 2:
                return "Bộ sưu tập";
                break;
            default:
                return "Tin tức";
        }
    };
};

var totalPage = 0;
var page = 1;
var limitPage = 10;
var dataPage = new Array();
var dataEdit;
var type;
var groupProductRespone;
var listParent = null;
var dataListGroupProduct = null;
var dataListCollection = null;
var dataListNewCategory = null;

$(document).ready(function() {
  page = $(location).attr('href').split("#")[2];
  setTimeout(function(){
    if (page === undefined) {
      page = 1;
      requestList(parseInt(page),parseInt(limitPage));
    } else {
      requestList(parseInt(page),parseInt(limitPage));
    }
  },100);
});


function requestList(pageNumber, limit){
  var dataRequest = new MenuRequest();
  dataRequest.page = pageNumber;
  dataRequest.limit = limit;
  setTimeout(function(){
      $.ajax({
      url: urlListMenu,
      type: 'post',
      dataType: 'json',
      contentType: 'application/json;charset=UTF-8',
      data: dataRequest.toJson()
          }).done(function (response, textStatus){
             groupProductRespone =  ResponeObject.fromRespone(response);
             console.log(groupProductRespone.toJson());
            $("#pageNumber").html("");
            $("#tableContent").html("");
            if (response.totalResult == 0) {
              $("#loading_view").hide();
            } else {
              dataPage = groupProductRespone.list;
              $("#totalResult").html("Hiển thị "+ response.list.length +" trên tổng số " + response.totalResult + " danh mục");
              if (parseInt(response.totalResult) <= parseInt(limit)) {
                $("#contentPageX").hide();
              } else {
                totalPage = parseInt(response.totalResult / limit);
                if (response.totalResult % limit !== 0) {
                  totalPage = totalPage + 1
                }
                 var backPage = pageNumber - 1;
                 var nextPage = pageNumber + 1;
                 if (pageNumber >= totalPage) {
                    nextPage = totalPage;
                 }
                 if (pageNumber <= 1) {
                    backPage = 1;
                 }
               
                $("#pageNumber").append('<li class="prev"><a href="#editMenu#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
                $("#pageNumber").append('<li class="prev"><a href="#editMenu#'+ backPage +'" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');
                for (var i = 1; i <= totalPage; i++) {
                  $("#pageNumber").append('<li><a href="#editMenu#'+i+'">'+ i +'</a></li>');
                }
                
                $("#pageNumber").append('<li class="next"><a href="#editMenu#'+ nextPage +'" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
                $("#pageNumber").append('<li class="next"><a href="#editMenu#'+ totalPage +'" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
              }
               
               $.each(groupProductRespone.list, function(i, item) {
                  var stt = (page - 1) * limit + i + 1;
                  var tmp =  '<tr class="odd gradeX"><td>'+ stt +'</td><td>'+ item.tieuDe +'</td>';
                      tmp += '<td>'+ new LoaiMenu(item.loaiMenu).toString() +'</td>';
                      tmp += '<td>'+ item.url +'</td>';
                      if (item.idCha === undefined) {
                          tmp += '<td></td>';
                      } else {
                          tmp += '<td>'+ item.idCha.tieuDe +'</td>';
                      }
                      tmp += '<td style="text-align:center;">'+ ((item.hienThi == 0) ? "<i class='glyphicon glyphicon-remove'style='color:red;'></i>" : "<i class='glyphicon glyphicon-ok' style='color:green;'></i>") +'</td>';
                      tmp += '<td style="text-align:center;"><a class="btn purple edit" data-toggle="modal" href="#large" onclick="editCategory('+ item.id +');">Sửa  <i class="fa fa-edit"></i></a>';
                      tmp += '<button onclick="deleteMenu('+ item.id +');" class="btn gray delete" attr-data="'+ item.id +'">Xóa  <i class="fa fa-trash-o"></i></button></td></tr>';
                  $("#tableContent").append(tmp);
              });
              $("#loading_view").hide()
            }
            requestListParent(null);
            requestListCategory(0,null);
            requestListCategory(1,null);
            requestListCategory(2,null);
           
       }).fail(function (){
         console.log( "loi" );
         $("#loading_view").hide();
       });
  },100);
 
}

function deleteMenu(e){
  if (confirm("Bạn có chắc chắn muốn xóa menu")) {
    var dataRequest = new MenuRequest();
    dataRequest.id = e;
    setTimeout(function(){
      $.ajax({
      url: urlDeleteMenu,
      type: 'post',
      dataType: 'json',
      contentType: 'application/json;charset=UTF-8',
      data: dataRequest.toJson()
          }).done(function (response, textStatus){
            console.log(response)
             if (response) {
                  requestList(parseInt(page),parseInt(limitPage));
             } else {
                $("#category_message").val("Xóa danh mục không thành công");
             }
           
       }).fail(function (){
         console.log( "loi" );
         $("#loading_view").hide();
       });
    },100);
  }
}

function clickAddNew(){
  dataEdit = null;
  type = "";
  $("#listCategory").html("");
  $("#listCategory").append(new Option("Chọn danh mục để hiển thị",-1));
  if (listParent == null) {
    requestListParent(function(){
      initListParent();
    });
  } else{
    initListParent();
  }
  seoTMP = new SeoRequest();
  $("#productMessage").html("(*) Phần bắt buộc phải nhập");
  $("#helperURL").html("");
    $("#dataEditName").val("");
    $("#dataEditURL").val("");
    $("#submit").html("Thêm mới");
    $(".modal-title").html("Thêm mới Menu");
    $("#txtMota").val("");
    $("#avatarProduct").attr("src","");
    $("#avatarImage").val("");
    $("#loaiMenu").html("");
    $("#loaiMenu").append(new Option("Chọn loại Menu",-1));
    $("#loaiMenu").append(new Option("Tin tức",0));
    $("#loaiMenu").append(new Option("Nhóm hàng hóa",1));
    $("#loaiMenu").append(new Option("Bộ sưu tập",2));
}

function seoEditor(){
    if ($("#dataEditURL").val().length == 0) {
      alert("Bạn phải điền nhập thông tin của menu trước");
  } else {
     $("#editSEO").modal();
    $(".modal-title-seo").html("Cấu hình SEO menu");
  $("#seoMessage").html("(*) Phần bắt buộc phải nhập");
  $("#helpSlug").html("");
  $("#seoType").val("0");
  if (seoTMP.id !== -1) {
    $("#seoTitle").val(seoTMP.title);
    $("#seoSlug").val(seoTMP.slug);
  } else {
    $("#seoTitle").val($("#dataEditName").val());
    $("#seoSlug").val($("#dataEditURL").val());
  }

  $("#seoDescription").val(seoTMP.description);
  $("#seoKeyWorks").val(seoTMP.keywords);
  $("#seoFavicon").attr("src", seoTMP.favico);
  $("#seoImage").attr("src", seoTMP.ogImage);
  $("#seoOGWIDTH").val(seoTMP.ogImageWidth);
  $("#seoOGHEIGHT").val(seoTMP.ogImageHeight);
  $("#seoOGSITE").val(seoTMP.ogImageHeight);
  $("#seoOGTYPE").val(seoTMP.ogType);
  $("#seoOGLOCALE").val(seoTMP.ogLocale);
  $("#seoTwitterCard").val(seoTMP.twitterCard);
  }
  
}

function editCategory(e){
  $.each(dataPage, function(i, item) {
    if (item.id === e) {
      dataEdit = item;
      type = "edit";
      $("#productMessage").html("(*) Phần bắt buộc phải nhập");
      $("#helperURL").html("");
         $("#dataEditName").val(dataEdit.tieuDe);
            $("#dataEditURL").val(dataEdit.url);
            $("#txtMota").val(dataEdit.moTa);
            $("#dataEditShow").prop('checked',(dataEdit.hienThi === 1));
            $("#avatarProduct").attr("src",dataEdit.icon);
            $(".modal-title").html("Chỉnh sửa Menu");
            $("#listCategory").html("");
            $("#listCategory").append(new Option("Chọn danh mục để hiển thị",-1));
            if (listParent == null) {
              requestListParent(function(){
                initListParent();
              });
            } else{
              initListParent();
            }
            $("#loaiMenu").html("");
            $("#loaiMenu").append(new Option("Chọn loại Menu",-1));
            $("#loaiMenu").append(new Option("Tin tức",0));
            $("#loaiMenu").append(new Option("Nhóm hàng hóa",1));
            $("#loaiMenu").append(new Option("Bộ sưu tập",2));
            $('#loaiMenu option[value='+ parseInt(item.loaiMenu) +']').attr('selected','selected');
            $("#submit").html("Sửa");
            initListCategory(parseInt(item.loaiMenu), function(isResult){
              $('#listCategory option[value='+ parseInt(item.idLoaiMenu) +']').attr('selected','selected');
            });
            requestRouteSeo("0", dataEdit.id, function(isResult){
              if (!isResult) {
                  $("#helperSEO").html("Chưa có cấu hình SEO");
                  $("#helperSEO").show();
              } else {
                  $("#helperSEO").hide();
              }
            });
    }
  });
}

function viewImage(e){
  $.each(dataPage, function(i, item) {
    if (item.id === e) {
      console.log(item);
      $("#imgSelected").attr("src", item.icon);
    }
  });
}

function deleteAll(){
  console.log("deleteAll");
}

$("#dataEditName").on('change keydown paste input', function(){
    $("#dataEditURL").val("mnu-" + removeCharacter($("#dataEditName").val()).toLowerCase().replaceAll(' ','-'));
    if (findUrl($("#dataEditURL").val())) {
      $("#helperURL").show();
      $("#helperURL").html("Link rút gọn đã tồn tại. Hệ thống sẽ tự động thêm hậu tố. Ví dụ: " + $("#dataEditURL").val() + "-1");
    } else {
      $("#helperURL").html("");
    }
});
$("#dataEditURL").on('change keydown paste input', function(){
    if (findUrl($("#dataEditURL").val())) {
      $("#helperURL").show();
      $("#helperURL").html("Link rút gọn đã tồn tại. Hệ thống sẽ tự động thêm hậu tố. Ví dụ: " + $("#dataEditURL").val() + "-1");
    } else if (checkRegexShortUrl($("#dataEditURL").val())) {
      $("#helperURL").hide();
    } else {
      $("#helperURL").html("Link rút gọn không được để trống và chỉ bao gồm các chữ cái từ a-z, số từ 0-9 và dấu '-'");
      $("#helperURL").show();
    }      
});

$("#submit").click(function(){
    var dataRequest = new MenuRequest();
    if (type=== "edit") {
      dataRequest.id = dataEdit.id;
    }
    dataRequest.idCha = $("#listParent").val();
    dataRequest.loaiMenu = $("#loaiMenu").val();
    dataRequest.tieuDe = $("#dataEditName").val();
    dataRequest.url = $("#dataEditURL").val().replaceAll("/","-").replaceAll("(","").replaceAll(")","").replaceAll("---","-").replaceAll("--","-").replaceAll(":","").replaceAll("&","");
    dataRequest.hienThi = ($('#dataEditShow').is(":checked") ? 1 : 0);
    dataRequest.icon = $("#avatarProduct").attr("src");
    dataRequest.idLoaiMenu = $("#listCategory").val();
    var countRoot = 0;
    if (listParent == null) {
      requestListParent(function(){
        $.each(listParent.list, function(i, item) {
          if (item.idCha === undefined) {
            countRoot += 1;
          }
        });
      });
    } else{
      $.each(listParent.list, function(i, item) {
          if (item.idCha === undefined) {
            countRoot += 1;
          }
      });
    }
    setTimeout(function() {
      if (dataRequest.loaiMenu == "-1") {
          $("#productMessage").html("Vui lòng chọn loại menu");
      } else if (dataRequest.idCha == "-1" && countRoot >= 7 && dataRequest.id == null) {
          $("#productMessage").html("Trang chủ nên hiển thị tối đa 7 menu, vui lòng chọn menu cha");
      } else if (seoTMP.slug == "") {
          $("#productMessage").html("Bạn chưa cấu hình SEO cho Menu");
      }else {
        $.ajax({
          url: urlCreateMenu,
          type: 'post',
          dataType: 'json',
          contentType: 'application/json;charset=UTF-8',
          data: dataRequest.toJson()
              }).done(function (response, textStatus){
                if (response.errorCode === null || response.errorCode === undefined || response.errorCode === "undefined") {
                  seoTMP.idLienKet  = response.id;
                  setTimeout(function(){
                    createRouteSeo(seoTMP, function(isResult){
                          seoObject = new SeoRequest();
                          requestList(parseInt(page),parseInt(limitPage));
                    });
                  },200);
                 $("#closePopup").click();
               } else {
                  $("#productMessage").html(response.msg);
               }
                
           }).fail(function (){
             console.log( "loi" );
           });
      }
    },200);
});

$("#closePopup").on('click',function(){
    seoObject = new SeoRequest();
});

$("#closePopup2").on('click',function(){
    seoObject = new SeoRequest();
});


function processCategory(loaiMenu, response) {
  $("#listCategory").html("");
  $("#listCategory").append(new Option("Tất cả " + (new LoaiMenu(loaiMenu).toString()), 0));
  if (response.totalResult == 0) {
    $("#listCategory").hide();
  } else {
    for (var i = 0; i < response.list.length; i++) {
      $("#listCategory").append(new Option(((loaiMenu == 2) ? response.list[i].tieuDe : response.list[i].ten ), response.list[i].id));
    }
  }
}

function initListCategory(loaiMenu, callback){
    var loaiMenu = parseInt(loaiMenu);
    if (loaiMenu != -1) {
      switch (loaiMenu){
        case 1:
          if (dataListGroupProduct === null){
            requestListCategory(loaiMenu, function(){
              processCategory(loaiMenu, dataListGroupProduct);
            });
          } else {
            processCategory(loaiMenu, dataListGroupProduct);
          }
          break;
        case 2:
          if (dataListCollection === null){
            requestListCategory(loaiMenu, function(){
              processCategory(loaiMenu, dataListCollection);
            });
          } else {
            processCategory(loaiMenu, dataListCollection);
          }
          break;
        default:
          if (dataListNewCategory === null){
            requestListCategory(loaiMenu, function(){
              processCategory(loaiMenu, dataListNewCategory);
            });
          } else {
            processCategory(loaiMenu, dataListNewCategory);
          }
      }

      $("#listCategory").show();
    } else {
      $("#listCategory").hide();
    }
    setTimeout(function(){
      if (callback != null) {
        callback();
      }
    },100);
}

function requestListCategory(loaiMenu, callback){
  loaiMenu = parseInt(loaiMenu);
  var url = "listCollection";
  switch (loaiMenu){
    case 1:
      url = "listGroupProduct";
      break;
    case 2:
      url = "listCollection";
      break;
    default:
      url = "listNewsCategory";
  }
  
  $.ajax({
    url: httpBaseRequest + url,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({MA_DVI:bussinessCode})
        }).done(function (response, textStatus){
          switch (loaiMenu){
            case 1:
              dataListGroupProduct = response;
              break;
            case 2:
              dataListCollection = response; 
              break;
            default:
              dataListNewCategory = response;
          }
          if (callback != null) {
            callback();
          }
          
     }).fail(function (){
        console.log( "loi requestListCategory" );
        if (callback != null) {
            callback();
        }
     });

}

$( "#loaiMenu" ).change(function() {
  initListCategory($("#loaiMenu").val(), null);
})


function requestListParent(callback){
  $.ajax({
    url: urlListMenu,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: new MenuRequest(null, null, null, null, null, null, null, null, bussinessCode, null, null, null).toJson()
        }).done(function (response, textStatus){
          listParent = response;
          if (callback != null) {
            callback();
          }
          
     }).fail(function (){
        console.log( "loi requestListParent" );
        if (callback != null) {
          callback();
        }
     });
}

function initListParent() {
  $("#listParent").html("");
  $("#listParent").append(new Option("Chọn Menu cha", -1));
  if (listParent.totalResult == 0) {
    $("#listParent").hide();
  } else {
    for (var i = 0; i < listParent.list.length; i++) {
      if(dataEdit === undefined || dataEdit === null){
        if (listParent.list[i].idCha === undefined) {
          $("#listParent").append(new Option(listParent.list[i].tieuDe, listParent.list[i].id));
        }
      } else {
          if(dataEdit.id != listParent.list[i].id && listParent.list[i].idCha === undefined){
            $("#listParent").append(new Option(listParent.list[i].tieuDe, listParent.list[i].id));
          } 
          if (dataEdit.idCha !== undefined){
            if (dataEdit.idCha.id == listParent.list[i].id) {
              $('#listParent >option[value='+ parseInt(dataEdit.idCha.id) +']').attr('selected','selected');
            }
          }
      }
    }
  }
}

function findUrl(url) {
  for (var i = 0; i < listParent.list.length; i++) {
    if (dataEdit != null) {
      if (listParent.list[i].url == url) {
        if (dataEdit.url != url) {
          return true;
        }
      }
    } else {
      if (listParent.list[i].url == url) {
        return true;
      }
    }
    
  }
  return false;
}