var urlListComments = httpBaseRequest + "getListComments";

var CommentsRequest = function () {

    this.page = -1;
    this.limit = -1;
    this.cmtCate = 1;
    this.maDviQly = bussinessCode;

    this.toJson = function () {
        return JSON.stringify(this);
    };
}

var totalPage = 0;
var page = $(location).attr('href').split("#")[2];
var limitPage = 10;
var dataPage = new Array();
var productRequest = new CommentsRequest();
var productRespone;
var dataEdit = null;
if (page === undefined) {
    page = 1;
    requestList(parseInt(page), parseInt(limitPage), $("#cmtCate option:selected").val());
} else {
    requestList(parseInt(page), parseInt(limitPage), $("#cmtCate option:selected").val());
}

$('#cmtCate').on('change', function() {
    requestList(parseInt(page), parseInt(limitPage), $("#cmtCate option:selected").val());
});

function requestList(pageNumber, limit, search) {
    productRequest = new CommentsRequest();
    productRequest.page = pageNumber;
    productRequest.limit = limit;
    productRequest.cmtCate = search;
    productRequest.maDviQly = bussinessCode;

    $.ajax({
        url: urlListComments,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: productRequest.toJson({page: pageNumber, limit: limit, cmtCate: search, maDviQly: bussinessCode})})
            .done(function (response, textStatus) {
                productRespone = ResponeObject.fromRespone(response);
                console.log(response);

                $("#pageNumber").html("");
                $("#tableContent").html("");

                if (response.totalResult == 0) {
                    $("#loading_view").hide();
                } else {
                    dataPage = productRespone.list;

                    $("#totalResult").html("Hiển thị " + response.list.length + " trên tổng số " + response.totalResult + " bình luận");

                    if (parseInt(response.totalResult) <= parseInt(limit)) {
                        $("#contentPageX").hide();
                    } else {
                        totalPage = parseInt(response.totalResult / limit);
                        if (response.totalResult % limit !== 0) {
                            totalPage = totalPage + 1
                        }
                        var backPage = pageNumber - 1;
                        var nextPage = pageNumber + 1;
                        if (pageNumber >= totalPage) {
                            nextPage = totalPage;
                        }
                        if (pageNumber <= 1) {
                            backPage = 1;
                        }

                        $("#pageNumber").append('<li class="prev"><a href="#commentList#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
                        $("#pageNumber").append('<li class="prev"><a href="#commentList#' + backPage + '" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');

                        if (totalPage > 6) {

                            if (page != 1) {
                                $("#pageNumber").append('<li><a href="#commentList#' + (parseInt(page) - 1) + '">...</a></li>');
                            }

                            if (page >= totalPage - 3) {
                                for (var i = (totalPage - 3); i <= totalPage; i++) {
                                    $("#pageNumber").append('<li><a href="#commentList#' + i + '">' + i + '</a></li>');
                                }
                            } else {
                                for (var i = page; i <= (parseInt(page) + 3); i++) {
                                    $("#pageNumber").append('<li><a href="#commentList#' + i + '">' + i + '</a></li>');
                                }
                                $("#pageNumber").append('<li><a href="#commentList#' + (parseInt(page) + 1) + '">...</a></li>');
                            }
                        } else {
                            for (var i = 1; i <= totalPage; i++) {
                                $("#pageNumber").append('<li><a href="#commentList#' + i + '">' + i + '</a></li>');
                            }
                        }

                        $("#pageNumber").append('<li class="next"><a href="#commentList#' + nextPage + '" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
                        $("#pageNumber").append('<li class="next"><a href="#commentList#' + totalPage + '" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
                    }
                    var settingTemplate = JSON.parse(localStorage.settingTemplate);

                    $.each(productRespone.list, function (i, item) {
                        var tmpPhanLoai = item.loaiDtuong == 2 ? "/san-pham/" : "/tin-tuc/";
                        var link = 'http://'+ settingTemplate.cfDomainSite + tmpPhanLoai + item.slug;
                        var tmp = '<tr class="odd gradeX"><td>' + ((page -1) * limit +  i + 1) + '</td><td>' + item.ten + '</td>';
                        tmp += '<td>' + item.noiDung + '</td>';
                        tmp += '<td><a target="_blank" href = "'+link+'">'+ link +'</a></td>';
                        tmp += '<td>' + 
                                ((item.kiemDuyet == 0) ? "<i class='glyphicon glyphicon-remove'style='color:red;'></i>" : 
                                "<i class='glyphicon glyphicon-ok' style='color:green;'></i>")
                             + '</td>';
                        tmp += '<td><a class="btn purple edit" data-toggle="modal" href="#commentBox" onclick="editComment(' + item.id + ');">Duyệt bài  <i class="fa fa-edit"></i></a></td></tr>';

                        $("#tableContent").append(tmp);
                    });
                    
                    $("#loading_view").hide();
                }
            }).fail(function () {
        console.log("loi");
        $("#loading_view").hide();
    });
}

$(document).ready(function () {
    CKEDITOR.replace('cmNoiDung', {
      // Define the toolbar groups as it is a more accessible solution.
      
      toolbarGroups: [{
          "name": "basicstyles",
          "groups": ["basicstyles"]
        },
        {
          "name": "document",
          "groups": ["mode"]
        }
      ],
      // Remove the redundant buttons from toolbar groups defined above.
      removeButtons: 'Save,Preview,Print,Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
    });
});
function editComment(id){
    $.each(dataPage, function(i, item) {
        if (item.id === id) {
            dataEdit = item;
            $("#cmTen").val(dataEdit.ten);
            $("#cmEmail").val(dataEdit.email);
            $("#a").prop('checked',(dataEdit.kiemDuyet === 1));
            CKEDITOR.instances.cmNoiDung.setData(dataEdit.noiDung);
        }
    });
}

$("#submit").click(function(){
    dataEdit.kiemDuyet = $("#a").is(":checked") ? 1 : 0; 
    $.ajax({
    url: urlUpdateComment,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(dataEdit)
        }).done(function (response, textStatus){
          console.log(response);
          if (response === true) {
            $("#cmTen").val("");
            $("#cmEmail").val("");
            CKEDITOR.instances.cmNoiDung.setData("");
            requestList(parseInt(page), parseInt(limitPage), $("#cmtCate option:selected").val());
            $("#closePopup").click();
          } 
     }).fail(function (){
       
     });
});

