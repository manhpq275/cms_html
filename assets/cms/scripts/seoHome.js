$("#submitHomeSEO").click(function(){
  console.log("click seo");
  // seoObject.id            = $("#seoIDPHANLOAI").val();
  // seoObject.phanLoai      = $("#seoIDPHANLOAI").val();
  // seoObject.idLienKet     = $("#seoIDLIENKET").val();
  if (!isCheckLink) {
    $("#seoMessage").html("Bạn chưa nhấn kiểm tra link rút gọn hoặc link rút gọn đã tồn tại");
  } else {
    seoTMP.phanLoai      = $("#seoHomeType").val();
    seoTMP.dviQly        = bussinessCode;
    seoTMP.slug          = $("#seoHomeSlug").val().replaceAll("/","-").replaceAll("(","").replaceAll(")","").replaceAll("---","-").replaceAll("--","-").replaceAll(":","").replaceAll("&","");
    seoTMP.title         = $("#seoHomeTitle").val();
    seoTMP.description   = $("#seoHomeDescription").val();
    seoTMP.keywords      = $("#seoHomeKeyWorks").val();
    seoTMP.favico        = $("#seoHomeFavicon").attr('src');
    seoTMP.ogSiteName    = $("#seoHomeOGSITE").val();
    seoTMP.ogTitle       = $("#seoHomeTitle").val();
    seoTMP.ogDescription = $("#seoHomeDescription").val();
    seoTMP.ogType        = $("#seoHomeOGTYPE").val();
    seoTMP.ogImage       = $("#seoHomeImage").attr('src');
    seoTMP.ogUrl         = $("#seoHomeSlug").val();
    seoTMP.ogLocale      = $("#seoHomeOGLOCALE").val();
    seoTMP.ogImageWidth  = $("#seoHomeOGWIDTH").val();
    seoTMP.ogImageHeight = $("#seoHomeOGHEIGHT").val();
    seoTMP.articleTag    = $("#seoHomeKeyWorks").val();
    seoTMP.twitterCard   = "summary";
    seoTMP.twitterDescription   = $("#seoHomeDescription").val();
    seoTMP.twitterImage         = $("#seoHomeImage").attr('src');
    seoTMP.twitterTitle   = $("#seoHomeTitle").val();
    if (isEmpty(seoTMP.title) || isEmpty(seoTMP.keywords)) {
      $("#seoHomeMessage").html("Không được để trống Tiêu đề hoặc Keywords");
    } else if (seoTMP.description.length > 500 ){
      $("#seoHomeMessage").html("Mô tả SEO tối đa 500 ký tự");
    } else {
        seoTMP.idLienKet = 0;
        createRouteSeo(seoTMP, function(data){
          if (data) {
            alert("Thêm SEO trang chủ thành công!");
          } else {
            alert("Thêm SEO trang chủ thất bại. Vui lòng thử lại!");
          }
        });
    }
  }
});

$("#checkHomeSlug").click(function(){
  requestService(urlGetSeoBySlug, JSON.stringify({maDviQly: bussinessCode, slug: $("#seoHomeSlug").val()}), function(data){
    if (data != false) {
      $("#seoHomeMessage").html("");
      if (data.totalResult > 0) {
        if (seoTMP.id !== -1 && data.list[0].id === seoTMP.id) {
          $("#helpHomeSlug").html("Bạn có thể sử dụng link: " + $("#seoHomeSlug").val());
          isCheckLink = true;
        } else {
          $("#helpHomeSlug").html("Link rút gọn đã tồn tại. Vui lòng sửa link rút gọn");
          isCheckLink = false;
        }
      } else {
        $("#helpHomeSlug").html("Bạn có thể sử dụng link: " + $("#seoHomeSlug").val());
        isCheckLink = true;
      }
    }
  });
});

$(document).ready(function() {
    requestRouteSeo("7", 0, function(isResult){
    if (!isResult) {
      $("#seoHomeTitle").val("");
      $("#seoHomeSlug").val("");
      $("#seoHomeDescription").val("");
    } else {
      $("#seoHomeTitle").val(seoTMP.title);
      $("#seoHomeSlug").val(seoTMP.slug);
      $("#seoHomeDescription").val(seoTMP.description);
      $("#seoHomeImage").attr("src", seoTMP.ogImage);
    }
    $("#seoHomeMessage").html("(*) Phần bắt buộc phải nhập");
    $("#seoHomeType").val("7");
    $("#helpSlug").html("");
    $("#seoHomeKeyWorks").val(seoTMP.keywords);
    $("#seoHomeFavicon").attr("src", seoTMP.favico);
    $("#seoHomeOGWIDTH").val(seoTMP.ogImageWidth);
    $("#seoHomeHomeOGHEIGHT").val(seoTMP.ogImageHeight);
    $("#seoHomeOGSITE").val(seoTMP.seoOGSITE);
    $("#seoHomeOGTYPE").val(seoTMP.ogType);
    $("#seoHomeOGLOCALE").val(seoTMP.ogLocale);
    $("#seoHomeArticleTAG").val(seoTMP.articleTag);
  });
});
