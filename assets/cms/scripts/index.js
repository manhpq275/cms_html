var ListTags = function () {
    this.page = -1;
    this.limit = -1;
    this.searchData = "";
    this.maDviQly = bussinessCode;
    this.toJson = function () {
        return JSON.stringify(this);
    };
};

$(document).ready(function() {
   requestAllTags(0, -1, "");
   requestSettingTemplate(null);
});

function requestAllTags(pageNumber, limit, search) {
    var tagsRequest = new ListTags();
    tagsRequest.page = pageNumber;
    tagsRequest.limit = limit;
    tagsRequest.searchData = removeCharacter(search);
    tagsRequest.maDviQly = bussinessCode;
    $.ajax({
        url: urlListTags,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: tagsRequest.toJson()})
        .done(function (response, textStatus) {
            if (response.totalResult !== undefined && response.totalResult != null && response.totalResult > 0) {
                console.log(response);
                var listTags = new Array();
                response.list.forEach(function(item, i){
                    listTags.push(item.keyword);
                });
                localStorage.allTags = JSON.stringify(listTags);
            }
        }).fail(function () {});
}

function autocomplete(inp, arr, onChange) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        var location = removeCharacter(arr[i].toUpperCase()).indexOf(removeCharacter(val.toUpperCase()));
        if ( location > -1) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          if (location > 0) {
            b.innerHTML = arr[i].substr(0, (location));
          }
          b.innerHTML += "<strong>" + arr[i].substr(location, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(location + val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + i + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              onChange(this.getElementsByTagName("input")[0].value);
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
/*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

function requestSettingTemplate(callback){

    $.ajax({
        url: urlSettingTemplate,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify({maDviQly: bussinessCode})})
    .done(function (response, textStatus) {
        console.log(response);
        var tmp = JSON.parse(response.list[0].config);
        if (tmp.cfOtherNews == null || tmp.cfOtherNews == undefined) {
          tmp.cfOtherNews = new Array();
        }
        if (tmp.cfBanners == null || tmp.cfBanners == undefined) {
          tmp.cfBanners = new Array();
        }
        if (tmp.cfBanks == null || tmp.cfBanks == undefined) {
          tmp.cfBanks = new Array();
        }
        localStorage.settingTemplate = JSON.stringify(tmp);
        
        if (callback != null) {
          callback();
        }
    }).fail(function () {
        console.log("loi");
        $("#loading_view").hide();
    });
}
