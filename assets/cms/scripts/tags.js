var dataEdit = null;
var type;

var Tags = function () {
    this.page = -1;
    this.limit = -1;
    this.searchData = "";
    this.maDviQly = bussinessCode;
    this.id = 0;
    this.keyword = "";
    this.description = "";
    this.toJson = function () {
        return JSON.stringify(this);
    };
};

var totalPage = 0;
var page = 1;
var limitPage = 10;
var dataPage = new Array();

$(document).ready(function() {
    seoTMP = new SeoRequest();
    page = $(location).attr('href').split("#")[2];
    $("#dataSearch").val(localStorage.searchTags === undefined ? "" : localStorage.searchTags);
    if (page === undefined) {
    page = 1;
        requestList(parseInt(page), parseInt(limitPage), $("#dataSearch").val());
    } else {
        requestList(parseInt(page), parseInt(limitPage), $("#dataSearch").val());
    }
});

function clickSearch() {
    //alert($("#customerRoot").val());
    page = 1;
    requestList(parseInt(page), parseInt(limitPage), $("#dataSearch").val());
}

$("#dataSearch").keypress(function (event) {
    if (event.which == 13) {
        //code here
        //13 represents Enter key
        requestList(parseInt(page), parseInt(limitPage), $("#dataSearch").val());
    }
});


function requestList(pageNumber, limit, search) {
    listCheck = new Array();
    localStorage.searchTags = search;
    var tagsRequest = new Tags();
    tagsRequest.page = pageNumber;
    tagsRequest.limit = limit;
    tagsRequest.searchData = removeCharacter(search);
    tagsRequest.maDviQly = bussinessCode;

    $.ajax({
        url: urlListTags,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: tagsRequest.toJson()})
            .done(function (response, textStatus) {
                console.log(response);
                $("#pageNumber").html("");
                $("#tableContent").html("");
                if (response.totalResult == 0) {
                    $("#loading_view").hide();
                } else {
                    dataPage = response.list;

                    $("#totalResult").html("Hiển thị " + response.list.length + " trên tổng số " + response.totalResult + " tin");

                 
                        $("#contentPageX").show();
                        totalPage = parseInt(response.totalResult / limit);
                        if (response.totalResult % limit !== 0) {
                            totalPage = totalPage + 1
                        }
                        var backPage = pageNumber - 1;
                        var nextPage = pageNumber + 1;
                        if (pageNumber >= totalPage) {
                            nextPage = totalPage;
                        }
                        if (pageNumber <= 1) {
                            backPage = 1;
                        }

                        $("#pageNumber").append('<li class="prev"><a href="#tags#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
                        $("#pageNumber").append('<li class="prev"><a href="#tags#' + backPage + '" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');

                        if (totalPage > 6) {

                            if (page != 1) {
                                $("#pageNumber").append('<li><a href="#tags#' + (parseInt(page) - 1) + '">...</a></li>');
                            }

                            if (page >= totalPage - 3) {
                                for (var i = (totalPage - 3); i <= totalPage; i++) {
                                    $("#pageNumber").append('<li><a href="#tags#' + i + '">' + i + '</a></li>');
                                }
                            } else {
                                for (var i = page; i <= (parseInt(page) + 3); i++) {
                                    $("#pageNumber").append('<li><a href="#tags#' + i + '">' + i + '</a></li>');
                                }
                                $("#pageNumber").append('<li><a href="#tags#' + (parseInt(page) + 1) + '">...</a></li>');
                            }
                        } else {
                            for (var i = 1; i <= totalPage; i++) {
                                $("#pageNumber").append('<li><a href="#tags#' + i + '">' + i + '</a></li>');
                            }
                        }

                        $("#pageNumber").append('<li class="next"><a href="#tags#' + nextPage + '" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
                        $("#pageNumber").append('<li class="next"><a href="#tags#' + totalPage + '" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
                 

                    $.each(response.list, function (i, item) {
                        
                        var tmp = '<tr class="odd gradeX"><td><input id = "'+ item.id +'"" onchange="checkClick('+ item.id +');" type="checkbox"></td>';
                            tmp += '<td style="text-align:center;" >' + ((page -1) * limit +  i + 1) + '</td><td>' + item.keyword + '</td>';
                            tmp += '<td><a class="btn purple edit" data-toggle="modal" href="#createNews" onclick="editNew(' + item.id + ');">Sửa  <i class="fa fa-edit"></i></a></td></tr>';

                            $("#tableContent").append(tmp);
                    });
                    $("#loading_view").hide();
                }
            }).fail(function () {
        console.log("loi");
        $("#loading_view").hide();
    });
}

function editNew(id){
    $.each(dataPage, function(i, item) {
        if (item.id === id) {
            dataEdit = item;
            $("#addSEO").show();
            showHideEdit(false);
            type = "edit";
            requestRouteSeo("6", dataEdit.id, function(isResult){
                if (!isResult) {
                  $("#helperSEO").html("Chưa có cấu hình SEO");
                  $("#helperSEO").show();
                } else {
                  $("#helperSEO").hide();
                }
              });
            $("#submit").html("Sửa");
            $("#tagKeyword").val(dataEdit.keyword);
            $("#tagDescription").val(dataEdit.description);
            $("#titleDetail").html("Chỉnh sửa");
            $("#tagsMessage").html("(*) : Bắt buộc phải nhập không được để trống");
      }
  });
}

function clickAddNew(){
    dataEdit = null;
    showHideEdit(false);
    type = "";
    $("#addSEO").hide();
    $("#submit").html("Thêm mới");
    $("#helperSEO").html("");
    $("#tagKeyword").val("");
    $("#tagDescription").val("");
    $("#titleDetail").html("Thêm mới Tag");
    $("#tagsMessage").html("(*) : Bắt buộc phải nhập không được để trống");
}


$("#deleteTag").click(function(){
    $('input[type=checkbox]').prop('checked',false);
    listCheck = new Array();
    listCheck.push(dataEdit.id);
    clickRemove();
});


$("#submit").click(function(){
    var tagsRequest = new Tags();
    if (dataEdit != null) {
        tagsRequest.id = dataEdit.id;
    }
    tagsRequest.keyword = $("#tagKeyword").val();
    tagsRequest.description = $("#tagDescription").val();
    requestExcute(urlCreateTags, tagsRequest.toJson(), function(data){
        console.log(data);
        if (data == true) {
            $('input[type=checkbox]').prop('checked',false);
            $("#removeTags").attr('disabled','disabled');
            requestList(parseInt(page), parseInt(limitPage), $("#dataSearch").val());
             if (dataEdit != null) {
                alert("Sửa thành công");
                setTimeout(function(){
                      seoTMP.idLienKet = dataEdit.id;
                      createRouteSeo(seoTMP, function(isResult){
                        seoObject = new SeoRequest();
                        console.log(data);
                        clickAddNew();
                      });
                    },200);
             } else {
                alert("Thêm mới thành công");
                clickAddNew();
             }
        } else {
            $("#tagsMessage").html(data.msg);
        }
        
    });
    
});

var listCheck = new Array();
function checkClick(value) {
  if (value === "ALL") {
    if ($("#chooseAll").is(":checked")) {
      $('input[type=checkbox]').prop('checked',true);
      listCheck = new Array();
      dataPage.forEach(function(item){
        listCheck.push(item.id);
    });
  } else {
      $('input[type=checkbox]').prop('checked',false);
      listCheck = new Array();
  }
} else {
    var divID = "#" + value;
    if ($(divID).prop('checked')){
        listCheck.push(value);
    } else {
        listCheck = arrayRemove(listCheck, value);
    }
}
if (listCheck.length >0) {
    $("#removeTags").removeAttr('disabled');
    if (listCheck.length == dataPage.length) {
        $('#chooseAll').prop('checked',true);
      } else {
        $('#chooseAll').prop('checked',false);
      }
} else {
    $("#removeTags").attr('disabled','disabled');

}
}

function arrayRemove(arr, value) {
   return arr.filter(function(ele){
       return ele != value;
   });

}

function clickRemove(){
  if (confirm('Bạn có chắc chắn muốn xóa '+ listCheck.length +' Tags?')) {
    requestService(urlDeleteTags, JSON.stringify({maDviQly: bussinessCode, listTagId: listCheck}), function(data){
        console.log(data);
         $('input[type=checkbox]').prop('checked',false);
          $("#removeTags").attr('disabled','disabled');
        requestList(parseInt(page), parseInt(limitPage), $("#dataSearch").val());
    });
  } 
}

$("#cancelEdit").click(function(){
  if (dataEdit != null) {
    showHideEdit(false);
    type = "edit";
    $("#submit").html("Sửa");
    $("#tagKeyword").val(dataEdit.keyword);
    $("#tagDescription").val(dataEdit.description);
    $("#titleDetail").html("Chỉnh sửa");
    $("#tagsMessage").html("(*) : Bắt buộc phải nhập không được để trống");
  } else {
    clickAddNew();
  }
});

function showHideEdit(isHide){
  if (isHide){
    if ($("#contentEdit").css("display") === "block"){
      $("#contentEdit").css("display","none");
    }
  } else {
    if ($("#contentEdit").css("display") === "none"){
      $("#contentEdit").css("display","block");
    }
  }
}

function seoEditor(){
  if ($("#tagKeyword").val().length == 0) {
      alert("Bạn phải điền thông tin TAGS trước");
      setTimeout(function(){
         $("#closePopupSEO").click();
      },300);
  } else {
    $("#seoMessage").html("(*) Phần bắt buộc phải nhập");
    $("#seoType").val("6");
    $("#helpSlug").html("");
    if (seoTMP.id !== -1) {
      $("#seoTitle").val(seoTMP.title);
      $("#seoSlug").val(seoTMP.slug);
      $("#seoDescription").val(seoTMP.description);
      $("#seoImage").attr("src", seoTMP.ogImage);

    } else {
      $("#seoTitle").val($("#tagKeyword").val());
      $("#seoSlug").val(removeCharacter('tag-' + $("#tagKeyword").val()).toLowerCase().replaceAll(' ','-'));
      $("#seoDescription").val($("#tagDescription").val());
    }

    $("#seoKeyWorks").val(seoTMP.keywords);
    $("#seoFavicon").attr("src", seoTMP.favico);
    $("#seoOGWIDTH").val(seoTMP.ogImageWidth);
    $("#seoOGHEIGHT").val(seoTMP.ogImageHeight);
    $("#seoOGSITE").val(seoTMP.seoOGSITE);
    $("#seoOGTYPE").val(seoTMP.ogType);
    $("#seoOGLOCALE").val(seoTMP.ogLocale);
    $("#seoArticleTAG").val(seoTMP.articleTag);
  }
}