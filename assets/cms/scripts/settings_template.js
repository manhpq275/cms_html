console.log("createSettingTemplate");
var SettingTemplate = function () {
    this.maDviQly = bussinessCode;
    this.cfDomainSite = "";
    this.cfSpecialSlug = "";
    this.cfHotLine = "";
    this.cfFBLink = "";
    this.cfYTLink = "";
    this.cfAbout = "";
    this.cfShops = "";
    this.cfOtherNews = new Array();
    this.cfBanners = new Array();
    this.cfBanks = new Array();
}

var OtherNews = function(){
    this.cfTitleNews = "";
    this.cfLink = "";
}

var Bank = function(){
    this.cfBankName = "";
    this.cfBankAddress = "";
    this.cfBankAccount = "";
    this.cfLogo = "";
}

var tmpSettingTemplate = new SettingTemplate();
var settingTemplate = new SettingTemplate();

function requestSetting() {
        settingTemplate = JSON.parse(localStorage.settingTemplate);
        tmpSettingTemplate = JSON.parse(localStorage.settingTemplate);
        $("#cfDomainSite").val(settingTemplate.cfDomainSite);
        $("#cfSpecialSlug").val(settingTemplate.cfSpecialSlug);
        $("#cfHotLine").val(settingTemplate.cfHotLine);
        $("#cfFBLink").val(settingTemplate.cfFBLink);
        $("#cfYTLink").val(settingTemplate.cfYTLink);
        $("#cfAbout").val(settingTemplate.cfAbout);
        $("#cfShops").val(settingTemplate.cfShops);
        initNotes(tmpSettingTemplate.cfOtherNews);
        initBanners(tmpSettingTemplate.cfBanners);
        initBanks(tmpSettingTemplate.cfBanks);
        $("#bannerImage").attr("src","");
        $("#bankImage").attr("src","");
        $("#cfOtherNews").val("");
        $("#cfBankName").val("");
        $("#cfBankAddress").val("");
        $("#cfBankAccount").val("");
        $("#cfOtherNewsTitle").val("");
         $("#cfSaveBank").html("Thêm mới"); 
         $("#cfSaveBanner").html("Thêm mới");
         $("#cfSaveNote").html("Thêm mới");
   
}

function initNotes(listNotes){
    $("#listFooter").html("");
    if (listNotes.length == 0) {
        var tmp = '<div class="form-group">';
        tmp += '<label class="col-md-3 control-label">Các bài viết khác:</label>';
        tmp += '<div class="col-md-9">';
        tmp += '<label class="control-label">Chưa có bài viết</label></div></div>';
        $("#listFooter").append(tmp);
    } else {
        listNotes.forEach(function(item, i){
            var tmp = '<div class="form-group">';
            if (i == 0) {
                tmp += '<label class="col-md-3 control-label">Các bài viết khác:</label>';
            } else {
                tmp += '<label class="col-md-3 control-label"></label>';
            }
            tmp += '<div class="col-md-9">';
            tmp += '<a href ="'+ item.cfLink +'" target="_blank" class="col-md-9 control-label" style="text-align:left;">'+ item.cfTitleNews + '</a>';
            tmp += '<label onClick = "onEditNote('+i+')" class="btn green circle" style="float: right;">Sửa</label>';
            tmp += '<label onClick = "onDeleteNote('+i+')" class="btn green circle" style="float: right; margin-right: 5px;">Xóa</label></div></div>';
            $("#listFooter").append(tmp);
        });
    }                      
}

function initBanners(listBanners){
    $("#listBanner").html("");
    if (listBanners.length == 0) {
        var tmp = '<div class="form-group">';
        tmp += '<label class="col-md-3 control-label">Danh sách banner:</label>';
        tmp += '<div class="col-md-9">';
        tmp += '<label class="control-label">Chưa có Banner</label></div></div>';
        $("#listBanner").append(tmp);
    } else {
        listBanners.forEach(function(item, i){
            var tmp = '<div class="form-group">';
            if (i == 0) {
                tmp += '<label class="col-md-3 control-label">Danh sách banner:</label>';
            } else {
                tmp += '<label class="col-md-3 control-label"></label>';
            }
            tmp += '<div class="col-md-9">';
            tmp += '<a href ="'+ item +'" target="_blank" class="col-md-9 control-label" style="text-align:left;">Banner '+ i + '</a>';
            tmp += '<label onClick = "onEditBanner('+i+')" class="btn green circle" style="float: right;">Sửa</label>';
            tmp += '<label onClick = "onDeleteBanner('+i+')" class="btn green circle" style="float: right; margin-right: 5px;">Xóa</label></div></div>';
            $("#listBanner").append(tmp);
        });
    }

}

function initBanks(listBanks){
    $("#listBank").html("");
    if (listBanks.length == 0) {
        var tmp = '<div class="form-group">';
        tmp += '<label class="col-md-3 control-label">Tài khoản ngân hàng:</label>';
        tmp += '<div class="col-md-9">';
        tmp += '<label class="control-label">Chưa có tài khoản nào</label></div></div>';
        $("#listBank").append(tmp);
    } else {
        listBanks.forEach(function(item, i){
            var tmp = '<div class="form-group">';
            if (i == 0) {
                tmp += '<label class="col-md-3 control-label">Tài khoản ngân hàng:</label>';
            } else {
                tmp += '<label class="col-md-3 control-label"></label>';
            }
            tmp += '<div class="col-md-9">';
            tmp += '<label class="col-md-9 control-label" style="text-align:left;">'+ item.cfBankName +' - '+ item.cfBankAddress +' - '+ item.cfBankAccount +'</label>';
            tmp += '<label onClick = "onEditBank('+i+')" class="btn green circle" style="float: right;">Sửa</label>';
            tmp += '<label onClick = "onDeleteBank('+i+')" class="btn green circle" style="float: right; margin-right: 5px;">Xóa</label></div></div>';
            $("#listBank").append(tmp);
        });
    }
}

$("#cfCancel").click(function(){
    $("#cfDomainSite").val(tmpSettingTemplate.cfDomainSite);
    $("#cfSpecialSlug").val(tmpSettingTemplate.cfSpecialSlug);
    $("#cfHotLine").val(tmpSettingTemplate.cfHotLine);
    $("#cfFBLink").val(tmpSettingTemplate.cfFBLink);
    $("#cfYTLink").val(tmpSettingTemplate.cfYTLink);
    $("#cfAbout").val(tmpSettingTemplate.cfAbout);
    $("#cfShops").val(tmpSettingTemplate.cfShops);
});

$("#cfSave").click(function () {
    settingTemplate.maDviQly = bussinessCode;
    settingTemplate.cfDomainSite = $("#cfDomainSite").val();
    settingTemplate.cfSpecialSlug = $("#cfSpecialSlug").val();
    settingTemplate.cfHotLine = $("#cfHotLine").val();
    settingTemplate.cfFBLink = $("#cfFBLink").val();
    settingTemplate.cfYTLink = $("#cfYTLink").val();
    settingTemplate.cfAbout = $("#cfAbout").val();
    settingTemplate.cfShops = $("#cfShops").val();

    saveSetting(settingTemplate);
});

function saveSetting(settingObject, callback){
    $.ajax({
        url: urlCreateSettingTemplate,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: JSON.stringify(settingObject)
    }).done(function (response, textStatus) {
        console.log("create CreateSettingSites: ");
        requestSettingTemplate(function(){
            requestSetting();
            if (callback != null) {
                callback(true);
            }
        });
    }).fail(function () {
        console.log("loi");
        if (callback != null) {
            callback(false);
        }
    });
}

$(document).ready(function() {
    requestSetting();
});

var positionBanner = -1;
var positionNote = -1;
var positionBank = -1;

function onEditBanner(position){
    tmpSettingTemplate.cfBanners.forEach(function(item, i){
        if (i == position) {
            positionBanner = position;
            $("#bannerImage").attr("src", item);
            $("#cfSaveBanner").html("Sửa");
        }
    });

}

function onEditNote(position){
    tmpSettingTemplate.cfOtherNews.forEach(function(item, i){
        if (i == position) {
            positionNote = position;
            $("#cfOtherNewsTitle").val(item.cfTitleNews);
            $("#cfOtherNews").val(item.cfLink);
            $("#cfSaveNote").html("Sửa");
        }
    });
}

function onDeleteBank(position){
    var result = new Array();
    positionBank = -1;
    tmpSettingTemplate.cfBanks.forEach(function(item, i){
        if (i != position) {
            result.push(item);
        }
    });
    tmpSettingTemplate.cfBanks = result;
    saveSetting(tmpSettingTemplate, function(data){
        if (data) {
            alert("Xóa thành công tài khoản ngân hàng");
        } else {
            alert("Xóa tài khoản ngân hàng không thành công");
        }
    });
}

function onDeleteBanner(position){
    var result = new Array();
    positionBanner = -1;
    tmpSettingTemplate.cfBanners.forEach(function(item, i){
        if (i != position) {
            result.push(item);
        }
    });
    tmpSettingTemplate.cfBanners = result;
    saveSetting(tmpSettingTemplate, function(data){
        if (data) {
            alert("Xóa link banner thành công");
        } else {
            alert("Xóa link banner không thành công");
        }
    });
}

function onDeleteNote(position){

    var result = new Array();
    positionNote = -1;
    tmpSettingTemplate.cfOtherNews.forEach(function(item, i){
        if (i != position) {
            result.push(item);
        }
    });
    tmpSettingTemplate.cfOtherNews = result;
    saveSetting(tmpSettingTemplate, function(data){
        if (data) {
            alert("Xóa link bài viết thành công");
        } else {
            alert("Xóa link bài viết không thành công");
        }
    });
}

function onEditBank(position){
    tmpSettingTemplate.cfBanks.forEach(function(item, i){
        if (i == position) {
            positionBank = position;
            $("#cfBankName").val(item.cfBankName);
            $("#cfBankAddress").val(item.cfBankAddress);
            $("#cfBankAccount").val(item.cfBankAccount);
            $("#bankImage").attr("src", item.cfLogo);
             $("#cfSaveBank").html("Sửa");
        }
    });
}

$("#cfSaveBanner").click(function(){
    if (positionBanner == -1) {
        if ($("#bannerImage").attr("src") === undefined || $("#bannerImage").attr("src") === "") {
            alert("Link banner không được để trống");
        } else {
            tmpSettingTemplate.cfBanners.push($("#bannerImage").attr("src"));
            saveSetting(tmpSettingTemplate, function(data){
                    positionBanner = -1;
                    if (data) {
                        alert("Thêm link banner thành công");
                    } else {
                        alert("Thêm link banner không thành công");
                    }
                });
        }
    } else {
        tmpSettingTemplate.cfBanners.forEach(function(item, i){
            if (i == positionBanner) {
                tmpSettingTemplate.cfBanners[positionBanner] = $("#bannerImage").attr("src");
                saveSetting(tmpSettingTemplate, function(data){
                    positionBanner = -1;
                    if (data) {
                        alert("sửa link banner thành công");
                    } else {
                        alert("sửa link banner không thành công");
                    }
                });
            }
        });
    }
});
$("#cfSaveNote").click(function(){
    if (positionNote == -1) {
        if ($("#cfOtherNews").val() === undefined || $("#cfOtherNews").val() === "") {
            alert("Link bài viết không được để trống");
        } else if ($("#cfOtherNewsTitle").val() === "") {
            alert("Tiêu đề bài viết không được để trống");
        } else {
            var otherNews = new OtherNews();
            otherNews.cfTitleNews = $("#cfOtherNewsTitle").val();
            otherNews.cfLink = $("#cfOtherNews").val();
            tmpSettingTemplate.cfOtherNews.push(otherNews);
            saveSetting(tmpSettingTemplate, function(data){
                    positionNote = -1;
                    if (data) {
                        alert("Thêm link bài viết thành công");
                    } else {
                        alert("Thêm link bài viết không thành công");
                    }
                });
        }
    } else {
        tmpSettingTemplate.cfOtherNews.forEach(function(item, i){
            if (i == positionNote) {
                tmpSettingTemplate.cfOtherNews[positionNote].cfTitleNews = $("#cfOtherNewsTitle").val();
                tmpSettingTemplate.cfOtherNews[positionNote].cfLink = $("#cfOtherNews").val();
                saveSetting(tmpSettingTemplate, function(data){
                    positionNote = -1;
                    if (data) {
                        alert("Sửa link bài viết thành công");
                    } else {
                        alert("Sửa link bài viết không thành công");
                    }
                });
            }
        });
    }

});

$("#cfSaveBank").click(function(){
    if ($("#cfBankName").val() == "") {
        alert("Tên chủ tài khoản không được để trống.");
    } else if ($("#cfBankAddress").val() == "") {
        alert("Địa chỉ ngân hàng không được để trống.");
    } else if ($("#cfBankAccount").val() == "") {
        alert("Số tài khoản không được để trống.");
    }  else if ($("#bankImage").attr('src') == "") {
        alert("Logo ngân hàng không được để trống");
    } else {
        if (positionBank == -1){
                    var bank = new Bank();
                    bank.cfBankName = $("#cfBankName").val();
                    bank.cfBankAddress = $("#cfBankAddress").val();
                    bank.cfBankAccount = $("#cfBankAccount").val();
                    bank.cfLogo = $("#bankImage").attr("src");
                    tmpSettingTemplate.cfBanks.push(bank);
                        saveSetting(tmpSettingTemplate, function(data){
                            positionBank = -1;
                            if (data) {
                                alert("Thêm tài khoản ngân hàng thành công");
                            } else {
                                alert("Thêm tài khoản ngân hàng không thành công");
                            }
                        });    
            } else {
                tmpSettingTemplate.cfBanks.forEach(function(item, i){
                    if (i == positionBank) {
                        tmpSettingTemplate.cfBanks[positionBank].cfBankName = $("#cfBankName").val();
                        tmpSettingTemplate.cfBanks[positionBank].cfBankAddress = $("#cfBankAddress").val();
                        tmpSettingTemplate.cfBanks[positionBank].cfBankAccount = $("#cfBankAccount").val();
                        tmpSettingTemplate.cfBanks[positionBank].cfLogo = $("#bankImage").attr("src");
                        saveSetting(tmpSettingTemplate, function(data){
                            positionBank = -1;
                            if (data) {
                                alert("Sửa tài khoản ngân hàng thành công");
                            } else {
                                alert("Sửa tài khoản ngân hàng không thành công");
                            }
                        });
                    }
                });
            }
    }
});

$("#cfCancelBank").click(function(){
    $("#bankImage").attr("src","");
    $("#cfBankName").val("");
    $("#cfBankAddress").val("");
    $("#cfBankAccount").val("");
    $("#cfSaveBank").html("Thêm mới");
    positionBank = -1;
});

$("#cfCancelBanner").click(function(){
    $("#bannerImage").attr("src","");
    $("#cfSaveBanner").html("Thêm mới");
    positionBanner = -1;
});

$("#cfCancelNote").click(function(){
    $("#cfOtherNews").val("");
    $("#cfOtherNewsTitle").val("");
    $("#cfSaveNote").html("Thêm mới");
    positionNote = -1;
});