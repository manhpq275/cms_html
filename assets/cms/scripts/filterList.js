var totalPage = 0;
var page = $(location).attr('href').split("#")[2];
var limitPage = 10;
var dataPage = new Array();
var dataEdit;
var type;
var groupProductRespone;
var listTT = null;
if (page === undefined) {
  page = 1;
  requestList(parseInt(page),parseInt(limitPage));
} else {
  requestList(parseInt(page),parseInt(limitPage));
}

function requestList(pageNumber, limit){
  $.ajax({
    url:urlListFilter ,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({maDviQly:bussinessCode, page: pageNumber, limit: limit})
        }).done(function (response, textStatus){
           groupProductRespone =  ResponeObject.fromRespone(response);
           console.log(groupProductRespone.toJson());
          $("#pageNumber").html("");
          $("#tableContent").html("");
          if (response.totalResult == 0) {
            $("#loading_view").hide();
          } else {
            dataPage = groupProductRespone.list;
            $("#totalResult").html("Hiển thị "+ response.list.length +" trên tổng số " + response.totalResult + " danh mục");
            if (parseInt(response.totalResult) <= parseInt(limit)) {
              $("#contentPageX").hide();
            } else {
              totalPage = parseInt(response.totalResult / limit);
              if (response.totalResult % limit !== 0) {
                totalPage = totalPage + 1
              }
               var backPage = pageNumber - 1;
               var nextPage = pageNumber + 1;
               if (pageNumber >= totalPage) {
                  nextPage = totalPage;
               }
               if (pageNumber <= 1) {
                  backPage = 1;
               }
             
              $("#pageNumber").append('<li class="prev"><a href="#filterList#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
              $("#pageNumber").append('<li class="prev"><a href="#filterList#'+ backPage +'" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');
              for (var i = 1; i <= totalPage; i++) {
                $("#pageNumber").append('<li><a href="#filterList#'+i+'">'+ i +'</a></li>');
              }
              
              $("#pageNumber").append('<li class="next"><a href="#filterList#'+ nextPage +'" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
              $("#pageNumber").append('<li class="next"><a href="#filterList#'+ totalPage +'" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
              console.log("totalPage: "+ totalPage);
            }
             
             $.each(response.list, function(i, item) {
                  $("#tableContent").append('<tr class="odd gradeX"><td>'+ item.tenBoLoc +'</td><td>'+ item.hinhThuc +'</td><td><a class="btn purple edit" data-toggle="modal" href="#large" onclick="editCategory('+ item.id +');" >Sửa  <i class="fa fa-edit"></i></a></center><button onclick="deleteCategory('+ item.id +');"  class="btn gray delete" attr-data="'+ item.id +'">Xóa  <i class="fa fa-trash-o"></i></button></td></tr>');
            });
            $("#loading_view").hide();
          }
          requestlistTT(null);
         
     }).fail(function (){
       console.log( "loi" );
       $("#loading_view").hide();
     });
}

function deleteCategory(e){
  if (confirm('Bạn có chắc chắn muốn xóa bộ lọc?')) {
    $.ajax({
    url: urlDeleteFilter,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({maDviQly:bussinessCode, id: e})
        }).done(function (response, textStatus){
          console.log(response)
           if (response === "true" || response == true) {
              alert("Xóa thành công");
              requestList(parseInt(page),parseInt(limitPage));
           } else {
              alert("Xóa không thành công");
              console.log("Xóa không thành công");
           }
         
     }).fail(function (){
       console.log( "loi" );
       $("#loading_view").hide();
     });
  } 
    
}

function clickAddNew(){
  dataEdit = null;
  type = "";
  $("#filterName").val("");
  $("#submit").html("Thêm mới");
  $(".modal-title").html("Thêm mới bộ lọc");
  $('#listOption >option[value="ALL"]').attr('selected','selected');
  // if (listTT == null) {
  //   requestlistTT(function(){
  //     initlistTT();
  //   });
  // } else{
  //   initlistTT();
  // }
}

function editCategory(e){
  $.each(dataPage, function(i, item) {
    if (item.id === e) {
      dataEdit = item;
      type = "edit";
      $("#submit").html("Sửa");
      $("#filterName").val(item.tenBoLoc);
      $("#filterCode").val(item.maBoLoc);
      $('#listOption >option[value="'+item.hinhThuc+'"]').attr('selected','selected');
     
      $(".modal-title").html("Chỉnh sửa Bộ lọc");
      $("#groupProductMessage").html("(*) : Bắt buộc phải nhập không được để trống");
      $("#filterSQL").val(item.truyVan);
    }
  });
}

function viewImage(e){
  $.each(dataPage, function(i, item) {
    if (item.id === e) {
      console.log(item);
      $("#imgSelected").attr("src", item.anh);
    }
  });
}

function deleteAll(){
  console.log("deleteAll");
}


function requestlistTT(callback){
  $.ajax({
    url: urlListTT,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({maDviQly:bussinessCode})
        }).done(function (response, textStatus){
          listTT = response;
          if (callback != null) {
            callback();
          }
          
     }).fail(function (){
        console.log( "loi requestlistTT" );
        if (callback != null) {
          callback();
        }
     });
}

function initlistTT() {
  $("#listTT").html("");
  $("#listTT").append(new Option("Chọn thuộc tính", "ALL"));
  $("#listTT").append(new Option("Giá tiền", "price"));
  if (listTT.totalResult == 0) {
    $("#listTT").hide();
  } else {
    for (var i = 0; i < listTT.list.length; i++) {
      if(dataEdit === undefined || dataEdit === null){
        if (listTT.list[i].idCha === undefined) {
          $("#listTT").append(new Option((listTT.list[i])[1], (listTT.list[i])[0]));
        }
      } else {
          if(dataEdit.id != listTT.list[i].id && listTT.list[i].idCha === undefined){
            $("#listTT").append(new Option((listTT.list[i])[1], (listTT.list[i])[0]));
          } 
          if (dataEdit.truyVan !== undefined){
            if (dataEdit.truyVan == (listTT.list[i])[0]) {
              $('#listTT >option[value="'+ dataEdit.truyVan +'""]').attr('selected','selected');
            }
          }
      }
    }
  }
}

$("#submit").click(function(){
  var dataRequest;
  if (type=== "edit") {
    dataRequest = {maDviQly:bussinessCode, maBoLoc : $("#filterCode").val(), id:dataEdit.id, hinhThuc: $("#listOption").val(), tenBoLoc: $("#filterName").val(), truyVan: $("#filterSQL").val()};
  }else{
    dataRequest = {maDviQly:bussinessCode, maBoLoc : $("#filterCode").val(), hinhThuc: $("#listOption").val(), tenBoLoc: $("#filterName").val(), truyVan: $("#filterSQL").val()};
  }
  if ($("#filterCode").val().length == 0) {
    $("#filterMessage").text("Mã thuộc tính không được để trống");
  } else if ($("#filterName").val().length == 0){
    $("#filterMessage").text("Tên bộ lọc không được để trống");
  } else if ($("#listOption").val() == "ALL"){
   $("#filterMessage").text("Hình thức lọc không được để trống");
 } else {
  $.ajax({
    url: urlCreateFilter,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(dataRequest)
  }).done(function (response, textStatus){
    if (response.errorCode === null || response.errorCode === undefined || response.errorCode === "undefined") {
     requestList(parseInt(page),parseInt(limitPage));
     $("#closePopup").click();
     if (type=== "edit") {
      alert("Sửa thành công");
    } else {
      alert("Thêm mới thành công");
    }
    
  } else {
    $("#filterMessage").html(response.msg);
  }
}).fail(function (){
 console.log( "loi" );
 if (type=== "edit") {
  alert("Sửa không thành công");
} else {
  alert("Thêm mới không thành công");
}
});
}

});
