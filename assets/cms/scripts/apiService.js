var urlListCollection = httpBaseRequest + "listCollection";
var urlCreateCollection = httpBaseRequest + "createCollection";
var urlDeleteCollection = httpBaseRequest + "deleteCollection";

var urlListProductInCollect = httpBaseRequest + "getListProductInCollect";
var urlDeleteProductInCollect = httpBaseRequest + "deleteProductInCollect";
var urlAddProductIntoCollect = httpBaseRequest + "addProductIntoCollect";

var urlListMenu = httpBaseRequest + "listMenu";
var urlCreateMenu = httpBaseRequest + "createMenu";
var urlDeleteMenu = httpBaseRequest + "deleteMenu";

var urlListGroupProduct = httpBaseRequest + "listGroupProduct";
var urlCreateGroupProduct = httpBaseRequest + "createGroupProduct";
var urlDeleteGroupProduct = httpBaseRequest + "deleteGroupProduct";

var urlListNewsCategory = httpBaseRequest + "listNewsCategory"
var urlCreatNewsCategory = httpBaseRequest + "createNewsCategory";
var urlDeleteNewsCategory = httpBaseRequest + "deleteNewsCategory";

var urlCreateProduct = httpBaseRequest + "createHangHoa";
var urlGetListProperty = httpBaseRequest + "getListPropertyProduct";

var urlSeoRequest = httpBaseRequest + "getRouteSeo";
var urlCreatSeoRequest = httpBaseRequest + "createRouteSeo";

var urlRootProduct = httpBaseRequest + "getRootProduct";
var urlProductDetail = httpBaseRequest + "getListProductDetail";
var urlListValue = httpBaseRequest + "getListValue";

var urlListTT = httpBaseRequest + "getListTT";
var urlListFilter = httpBaseRequest + "getListFilter";
var urlCreateFilter = httpBaseRequest + "createFilter";
var urlDeleteFilter = httpBaseRequest + "deleteFilter";

var urlCreateNews = httpBaseRequest + "creatNews";
var urlListProperty = httpBaseRequest + "getListProperty";

var urlUpdateComment = httpBaseRequest + "updateComment";
var urlListProductInGroup = httpBaseRequest + "listProductInGroupCMS";
var urlDeleteProductInGroup = httpBaseRequest + "deleteProductIntoGroup";
var urlAddProductIntoGroup = httpBaseRequest + "addProductIntoGroup";

var urlGetPriceByHHCT = httpBaseRequest + "getPriceByHHCTID";

var urlListTags     = httpBaseRequest + "getListTags";
var urlDeleteTags   = httpBaseRequest + "deleteTags";
var urlCreateTags   = httpBaseRequest + "updateTags";

var urlDeleteProduct = httpBaseRequest + "deleteProduct";
var urlGetSeoBySlug = httpBaseRequest + "getSeoBySlug";

var urlSettingTemplate = httpBaseRequest + "getSettingTemplate";
var urlCreateSettingTemplate = httpBaseRequest + "createSettingTemplate";

var urlListNews = httpBaseRequest + "getListNews";
var urlDeleteNew=httpBaseRequest + "deleteNews";

var urlListNewsIntoGroup = httpBaseRequest + "getListNewsIntoGroup";
var urlDeleteNewsIntoGroup = httpBaseRequest + "deleteNewsIntoGroup";
var urlAddNewsIntoGroup = httpBaseRequest + "addNewsIntoGroup";

function requestService(urlRequest,dataRequest,callBack){
  $.ajax({
    url: urlRequest,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: dataRequest
        }).done(function (response, textStatus){
          console.log(response);
          if (response.totalResult === undefined || response.totalResult === null) {
            callBack(false);
          } else {
            callBack(response);
          }
         
     }).fail(function (){
       console.log( "Error Request: "+ urlRequest);
       callBack(false);
     });
}

function requestExcute(urlRequest,dataRequest,callBack){
  $.ajax({
    url: urlRequest,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: dataRequest
        }).done(function (response, textStatus){
            callBack(response);
         
     }).fail(function (){
       console.log( "Error Request: "+ urlRequest);
       callBack(false);
     });
}

