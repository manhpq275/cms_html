$("#category_message").hide();

var totalPage = 0;
var page = $(location).attr('href').split("#")[2];
var limitPage = -1;
var dataPage = new Array();
var dataEdit;
var type;
var groupProductRespone;
var listParent = null;
var listProduct = null;
if (page === undefined) {
  page = 1;
  requestList(parseInt(page),parseInt(limitPage));
} else {
  requestList(parseInt(page),parseInt(limitPage));
}
$("#removeProduct").hide();
var Node = function (item){
  this.text = item.ten;
  this.data = item;
  this.children = new Array();
  this.children_d = new Array();
}

var listNode = new Array();

$(document).ready(function() {
  setTimeout(function(){
   initToTreeNode();
 }, 100);
});

function initToTreeNode(){
 $("#treeGroup").jstree({
  "core" : {
    "themes" : {
      "responsive": true
    }, 
                // so that create works
                "check_callback" : true,
                'data' : listNode
              },
              "types" : {
                "default" : {
                  "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                  "icon" : "fa fa-file icon-state-warning icon-lg"
                }
              },
              "plugins" : ["types" ]
            });
}

function requestList(pageNumber, limit){
  showHideEdit(true);
  $.ajax({
    url: httpBaseRequest + "listGroupProduct",
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({MA_DVI:bussinessCode, page: pageNumber, limit: limit})
  }).done(function (response, textStatus){
   groupProductRespone =  ResponeObject.fromRespone(response);
   console.log(groupProductRespone.toJson());
   if (response.totalResult == 0) {
    $("#loading_view").hide();
  } else {
    listNode = new Array();
    response.list.forEach(function(item) {
      if (item.idCha === undefined) {
        var isAlive = false;
        listNode.forEach(function(node){
          if (node.data.id == item.id) {
            isAlive = true;
          }
        });
        if (!isAlive) {
          listNode.push(new Node(item));
        }
      } else {
        if (listNode.length == 0) {
          var parentNode = new Node(item.idCha);
          parentNode.children.push(new Node(item));
          listNode.push(parentNode);
        } else {
          var isAlive = false;
          listNode.forEach(function(node){
            if (node.data.id == item.idCha.id) {
              isAlive = true;
              node.children.push(new Node(item));
            }
          });
          if (!isAlive) {
            var parentNode = new Node(item.idCha);
            parentNode.children.push(new Node(item));
            listNode.push(parentNode);
          }
        }
      }
    });
    setTimeout(function(){
      $('#treeGroup').jstree(true).settings.core.data = listNode;
      $('#treeGroup').jstree(true).refresh();
      $("#loading_view").hide();
    },200);
    
  }
  requestListParent(null);

}).fail(function (){
 console.log( "loi" );
 $("#loading_view").hide();
});
}

$("#deleteGroup").click(function(){

 if (confirm('Bạn có chắc chắn muốn xóa nhóm "'+ dataEdit.ten +'"?')) {
  if ($("#listParent").prop('disabled')) {
    alert("Bạn phải xóa hết nhóm con trước!");
  } else {
    $.ajax({
      url: httpBaseRequest + "deleteGroupProduct",
      type: 'post',
      dataType: 'json',
      contentType: 'application/json;charset=UTF-8',
      data: JSON.stringify({MA_DVI:bussinessCode, id: dataEdit.id})
    }).done(function (response, textStatus){
      console.log(response)
      if (response === "true" || response === true) {
        requestList(parseInt(page),parseInt(limitPage));
        clickAddNew();
      } 
    }).fail(function (){
     console.log( "loi" );
     $("#loading_view").hide();
   });
  }
}

});

function clickAddNew(){
  dataEdit = null;
  type = "";
  showHideEdit(false);
  showHideProducts(true)
  seoObject = new SeoRequest();
  seoTMP = new SeoRequest();
  $("#helperSEO").html("<font color='red'>Chưa có cấu hình SEO</font>");
  $("#helperSEO").show();
  $("#categoryName").val("");
  $("#submit").html("Thêm mới");
  $("#titleDetail").html("Thêm mới");
  $("#txtMota").val("");
  $("#avatarProduct").attr("src","");
  $("#avatarImage").val("");
  $("#listParent").prop('disabled', false);
  if (listParent == null) {
    requestListParent(function(){
      initListParent();
    });
  } else{
    initListParent();
  }
}

function editCategory(e){
  $.each(dataPage, function(i, item) {
    if (item.id === e) {

    }
  });
}

function viewImage(e){
  $.each(dataPage, function(i, item) {
    if (item.id === e) {
      console.log(item);
      $("#imgSelected").attr("src", item.anh);
    }
  });
}

function deleteAll(){
  console.log("deleteAll");
}


function requestListParent(callback){
  $.ajax({
    url: urlListGroupProduct,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({MA_DVI:bussinessCode})
  }).done(function (response, textStatus){
    listParent = response;
    if (callback != null) {
      callback();
    }

  }).fail(function (){
    console.log( "loi requestListParent" );
    if (callback != null) {
      callback();
    }
  });
}

function initListParent() {
  $("#listParent").html("");
  $("#listParent").append(new Option("Chọn Menu cha", -1));
  if (listParent.totalResult == 0) {
    $("#listParent").hide();
  } else {
    for (var i = 0; i < listParent.list.length; i++) {
      if(dataEdit === undefined || dataEdit === null){
        if (listParent.list[i].idCha === undefined) {
          $("#listParent").append(new Option(listParent.list[i].ten, listParent.list[i].id));
        }
      } else {
        if(dataEdit.id != listParent.list[i].id && listParent.list[i].idCha === undefined){
          $("#listParent").append(new Option(listParent.list[i].ten, listParent.list[i].id));
        } 
        if (dataEdit.idCha !== undefined){
          if (dataEdit.idCha.id == listParent.list[i].id) {
            $('#listParent >option[value='+ parseInt(dataEdit.idCha.id) +']').attr('selected','selected');
          }
        }
      }
    }
  }
}

$("#submit").click(function(){
  console.log($("#categoryName").val());
  console.log($("#listParent").val());
  var dataRequest;
  if (type=== "edit") {
    dataRequest = {MA_DVI:bussinessCode , ten: $("#categoryName").val(), groupCha: $("#listParent").val(), id:dataEdit.id, anh: $("#avatarProduct").attr("src"), moTa: $("#txtMota").val()}
  }else{
    dataRequest = {MA_DVI:bussinessCode , ten: $("#categoryName").val(), groupCha: $("#listParent").val(), anh: $("#avatarProduct").attr("src"), moTa: $("#txtMota").val()}
  }

  if (seoTMP.title === "") {
    $("#groupProductMessage").html("<font color='red'>Chưa có cấu hình SEO</font>");
    $("#groupProductMessage").show();
  } else {
    $.ajax({
      url: httpBaseRequest + "createGroupProduct",
      type: 'post',
      dataType: 'json',
      contentType: 'application/json;charset=UTF-8',
      data: JSON.stringify(dataRequest)
    }).done(function (response, textStatus){
      if (response.errorCode === null || response.errorCode === undefined || response.errorCode === "undefined") {
        if (type=== "edit") {
          alert("Sửa thành công");
        } else {
          alert("Thêm thành công");
        }
        
        setTimeout(function(){
          seoTMP.idLienKet = response.id;
          createRouteSeo(seoTMP, function(isResult){
            seoObject = new SeoRequest();
            requestList(parseInt(page),parseInt(limitPage));

          });
        },200);
        requestList(parseInt(page),parseInt(limitPage));

      } else {
        $("#groupProductMessage").html(response.msg);
      }
    }).fail(function (){
     console.log( "loi" );
   });
  }
});

function seoEditor(){
  $(".modal-title-seo").html("Cấu hình SEO nhóm hàng hóa");
  $("#seoMessage").html("(*) Phần bắt buộc phải nhập");
  $("#helpSlug").html("");
  $("#seoType").val("2");
  if (seoTMP.id !== -1) {
    $("#seoTitle").val(seoTMP.title);
    $("#seoSlug").val(seoTMP.slug);
    $("#seoDescription").val(seoTMP.description);
    $("#seoImage").attr("src", seoTMP.ogImage);

  } else {
    $("#seoTitle").val($("#categoryName").val());
    $("#seoSlug").val(removeCharacter('nhh-' + $("#categoryName").val()).toLowerCase().replaceAll(' ','-'));
    $("#seoDescription").val($("#txtMota").val());
    $("#seoImage").attr("src", $("#avatarProduct").attr('src'));
  }

  $("#seoKeyWorks").val(seoTMP.keywords);
  $("#seoFavicon").attr("src", seoTMP.favico);
  $("#seoOGWIDTH").val(seoTMP.ogImageWidth);
  $("#seoOGHEIGHT").val(seoTMP.ogImageHeight);
  $("#seoOGSITE").val(seoTMP.seoOGSITE);
  $("#seoOGTYPE").val(seoTMP.ogType);
  $("#seoOGLOCALE").val(seoTMP.ogLocale);
  $("#seoArticleTAG").val(seoTMP.articleTag);
}

$('#treeGroup').on('select_node.jstree', function(e,data) { 
  showHideEdit(false);
  dataEdit = data.node.data;
  type = "edit";
  requestListProductInGroup(dataEdit.id);
  $("#submit").html("Sửa");
  $("#categoryName").val(dataEdit.ten);
  $("#txtMota").val(dataEdit.moTa);
  $("#avatarProduct").attr("src",dataEdit.anh);
  $("#titleDetail").html("Chỉnh sửa");
  $("#groupProductMessage").html("(*) : Bắt buộc phải nhập không được để trống");
  seoTMP = new SeoRequest();
  seoObject = new SeoRequest();
  requestRouteSeo("2", dataEdit.id, function(isResult){
    if (!isResult) {
      $("#helperSEO").html("Chưa có cấu hình SEO");
      $("#helperSEO").show();
    } else {
      $("#helperSEO").hide();
    }
  });
  if (data.node.children.length > 0) {
    initListParent();
    $("#listParent").prop('disabled', true);
  } else {
    $("#listParent").prop('disabled', false);
    if (listParent == null) {
      requestListParent(function(){
        initListParent();
      });
    } else{
      initListParent();
    }
  }
});

$("#cancelEdit").click(function(){
  if (dataEdit != null) {
    showHideEdit(false);
    type = "edit";
    requestListProductInGroup(dataEdit.id);
    $("#submit").html("Sửa");
    $("#categoryName").val(dataEdit.ten);
    $("#txtMota").val(dataEdit.moTa);
    $("#avatarProduct").attr("src",dataEdit.anh);
    $("#titleDetail").html("Chỉnh sửa");
    $("#groupProductMessage").html("(*) : Bắt buộc phải nhập không được để trống");
    seoTMP = new SeoRequest();
    seoObject = new SeoRequest();
    requestRouteSeo("2", dataEdit.id, function(isResult){
      if (!isResult) {
        $("#helperSEO").html("Chưa có cấu hình SEO");
        $("#helperSEO").show();
      } else {
        $("#helperSEO").hide();
      }
    });
  } else {
    clickAddNew();
  }
});

function showHideEdit(isHide){
  if (isHide){
    if ($("#contentEdit").css("display") === "block"){
      $("#contentEdit").css("display","none");
    }
  } else {
    if ($("#contentEdit").css("display") === "none"){
      $("#contentEdit").css("display","block");
    }
  }
}

function showHideProducts(isHide){
  if (isHide){
    if ($("#productContent").css("display") === "block"){
      $("#productContent").css("display","none");
    }
  } else {
    if ($("#productContent").css("display") === "none"){
      $("#productContent").css("display","block");
    }
  }
}

function initList(list){
  $("#totalResult").html("Có tất cả "+ list.length +" sản phẩm");
  list.forEach(function(item, i) {
    var stt =  i + 1;
    var tmp =  '<tr class="odd gradeX">';
    tmp +=  '<td><input id = "'+ item.idHhCt.id +'"" onchange="checkClick('+ item.idHhCt.id +');" type="checkbox"></td>';
    tmp += '<td>'+ stt +'</td>';
    tmp += '<td>'+ item.idHhCt.tenHang +'</td>';
    tmp += '<td>'+ item.idHhCt.maVach +'</td>';
    $("#tableContent").append(tmp);
  });
}

var requestProduct = null;
function requestListProductInGroup(idGroup){
  $('#chooseAll').prop('checked',false);
  listCheck = new Array();
  if (requestProduct != null) {
    requestProduct.abort();
  }
  listProduct = new Array();
  showHideProducts(true);
  $("#tableContent").html("");
  requestProduct = $.ajax({
    type: "POST",
    url: urlListProductInGroup,
    data: JSON.stringify({groupId: idGroup}),
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
  }).done(function (response, textStatus){
          if (response.totalResult === undefined || response.totalResult === 0) {
            $("#loading_view").hide();
            showHideProducts(false);
            $("#bodyProduct").css('height', "0px");
            $("#totalResult").html("Chưa có sản phẩm nào");
          } else {
            showHideProducts(false);
            $("#bodyProduct").css('height', "500px");
            listProduct = response.list;
            initList(response.list);
          }

  }).fail(function (){
    showHideProducts(false);
    $("#bodyProduct").css('height', "0px");
    $("#totalResult").html("Chưa có sản phẩm nào");
  });
}

var listCheck = new Array();
function checkClick(value) {
  if (value === "ALL") {
    if ($("#chooseAll").is(":checked")) {
      $('input[type=checkbox]').prop('checked',true);
      listCheck = new Array();
      listProduct.forEach(function(item){
        listCheck.push(item.idHhCt.id);
      });
    } else {
      $('input[type=checkbox]').prop('checked',false);
      listCheck = new Array();
    }
  } else {
    var divID = "#" + value;
    if ($(divID).prop('checked')){
        listCheck.push(value);
    } else {
        listCheck = arrayRemove(listCheck, value);
    }
  }
   if (listCheck.length >0) {
      if (listCheck.length == listProduct.length) {
        $('#chooseAll').prop('checked',true);
      } else {
        $('#chooseAll').prop('checked',false);
      }
      $("#removeProduct").show();
    } else {
      $("#removeProduct").hide();
    }
}

function arrayRemove(arr, value) {
   return arr.filter(function(ele){
       return ele != value;
   });

}

function clickRemove(){
  if (confirm('Bạn có chắc chắn muốn xóa '+ listCheck.length +' sản phẩm ra khỏi nhóm?')) {
    requestService(urlDeleteProductInGroup, JSON.stringify({groupId: dataEdit.id, listProductId: listCheck}), function(data){
        requestListProductInGroup(dataEdit.id);
    });
  } 
}

var listAllProduct = new Array();
var listAllTMP = new Array();
requestAllProduct();

function addProductGroup(){
  if (listAllProduct.length == 0) {
    $(".modal-title").text("Chọn thêm sản phẩm vào nhóm");
  } else {
    $(".modal-title").text("Chọn thêm sản phẩm vào nhóm");
    $('input:checkbox.addProduct').prop('checked',false);
    $("#searchProduct").val("");
    listAllTMP = listAllProduct;
    if (listProduct != null && listProduct.length > 0) {
      listProduct.forEach(function(item){
        listAllTMP = filterAllProduct(listAllTMP, item.idHhCt.id)
      });
      console.log("listAllTMP.length = " + listAllTMP.length);
      initListAllProduct(listAllTMP);
    } else {
      $(".modal-title").text("Chọn thêm sản phẩm vào nhóm");
      initListAllProduct(listAllTMP);
    }
  }
}


var listAddCheck = new Array();
function checkAddClick(value) {
  if (value === "ALL") {
    if ($("#chooseAddAll").is(":checked")) {
      $('input:checkbox.addProduct').prop('checked',true);
      listAddCheck = new Array();
      listAllTMP.forEach(function(item){
        listAddCheck.push(item.dmHhoaCtTtinhBsung.dmHangHoaCt.id);
      });
    } else {
      $('input:checkbox.addProduct').prop('checked',false);
      listAddCheck = new Array();
    }
  } else {
    var divID = "#" + value;
    if ($(divID).prop('checked')){
        listAddCheck.push(value);
    } else {
        listAddCheck = arrayRemove(listAddCheck, value);
    }
    if (listAddCheck.length == listAllTMP.length) {
        $('#chooseAddAll').prop('checked',true);
      } else {
        $('#chooseAddAll').prop('checked',false);
      }
  }
  $("#totalChoose").html("Đã chọn: "+ listAddCheck.length +" sản phẩm");
}


function requestAllProduct(){
    $(".modal-title").text("Đang tải danh sách sản phẩm...");
    $.ajax({
      url: urlProductDetail,
      type: 'post',
      dataType: 'json',
      contentType: 'application/json;charset=UTF-8',
      data: JSON.stringify({maDviQly: bussinessCode, page :0, limit: -1})
    }).done(function (response, textStatus){
        console.log(response);
        listAllProduct = response.list;
        addProductGroup();
    }).fail(function (){
       console.log( "loi" );
       addProductGroup();
       $("#loading_view").hide();
     });
}

function filterAllProduct(list, value){
  var listResume = new Array();
  list.forEach(function(item){
      if (item.dmHhoaCtTtinhBsung.dmHangHoaCt.id != value) {
          listResume.push(item);
      }
  });
  return listResume;
}

function initListAllProduct(list){
  listAddCheck = new Array();
  $("#totalChoose").html("Đã chọn: "+ listAddCheck.length +" sản phẩm");
  $("#tableContentAdd").html("");
  $("#totalResultAdd").html("Có tất cả "+ list.length +" sản phẩm");
  list.forEach(function(item, i) {
    var stt =  i + 1;
    var tmp =  '<tr class="odd gradeX">';
    tmp +=  '<td><input id = "'+ item.dmHhoaCtTtinhBsung.dmHangHoaCt.id +'"" class = "addProduct" onchange="checkAddClick('+ item.dmHhoaCtTtinhBsung.dmHangHoaCt.id +');" type="checkbox"></td>';
    tmp += '<td>'+ stt +'</td>';
    tmp += '<td>'+ item.dmHhoaCtTtinhBsung.dmHangHoaCt.tenHang +'</td>';
    tmp += '<td>'+ item.dmHhoaCtTtinhBsung.dmHangHoaCt.maVach +'</td>';
    $("#tableContentAdd").append(tmp);
  });
}

function searchProduct (list, value){
  var listResume = new Array();
  list.forEach(function(item){
    var ten = "" + item.dmHhoaCtTtinhBsung.dmHangHoaCt.tenHang;
    var maVach = "" + item.dmHhoaCtTtinhBsung.dmHangHoaCt.maVach;
    var tmp = "" + value;
    var location1 = removeCharacter(ten.toUpperCase()).indexOf(removeCharacter(tmp.toUpperCase()));
    var location2 = removeCharacter(maVach.toUpperCase()).indexOf(removeCharacter(tmp.toUpperCase()));
      if (location1 != -1 || location2 != -1) {
          listResume.push(item);
      }
  });
  return listResume;
}
$("#searchProduct").on('input', function(){
  if ( $("#searchProduct").val() === "") {
    addProductGroup();
  } else {
    listAllTMP = searchProduct(listAllTMP, $("#searchProduct").val());
    initListAllProduct(listAllTMP);
  }
    
});

function clickAddNewProduct(){
  requestService(urlAddProductIntoGroup, JSON.stringify({groupId: dataEdit.id, listProductId: listAddCheck}), function(data){
    requestListProductInGroup(dataEdit.id);
    $("#closePopup").click();
    alert("Thêm thành công");
  });
}