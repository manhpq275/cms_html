var urlListSettingSites = httpBaseRequest + "getListSettingSites";
var urlCreateSettingSites = httpBaseRequest + "createSettingSites";
var urlDeleteSettingSites = httpBaseRequest + "deleteSettingSites";

var SettingSitesRequest = function () {

    this.page = -1;
    this.limit = -1;

    this.dviQly = bussinessCode;
    this.domainName = "";
    this.ipPublic = "";
    this.header = "";
    this.footer = "";
    this.trangThai = 1;
    this.id = 0;

    this.toJson = function () {
        return JSON.stringify(this);
    };
}

var totalPage = 0;
var page = $(location).attr('href').split("#")[2];
var limitPage = 50;
var dataPage = new Array();
var productRequest = new SettingSitesRequest();
var productRespone;
var listProperties = new Array();
var type;

if (page === undefined) {
    page = 1;
    requestList(parseInt(page), parseInt(limitPage));
} else {
    requestList(parseInt(page), parseInt(limitPage));
}

function requestList(pageNumber, limit) {
    productRequest = new SettingSitesRequest();
    productRequest.page = pageNumber;
    productRequest.limit = limit;

    $.ajax({
        url: urlListSettingSites,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: productRequest.toJson({page: pageNumber, limit: limit})})
            .done(function (response, textStatus) {
                productRespone = ResponeObject.fromRespone(response);
                console.log(response);

                $("#pageNumber").html("");
                $("#tableContent").html("");

                if (response.totalResult == 0) {
                    $("#loading_view").hide();
                } else {
                    dataPage = productRespone.list;

                    $("#totalResult").html("Hiển thị " + response.list.length + " trên tổng số " + response.totalResult + " ");

                    if (parseInt(response.totalResult) <= parseInt(limit)) {
                        $("#contentPageX").hide();
                    } else {
                        totalPage = parseInt(response.totalResult / limit);
                        if (response.totalResult % limit !== 0) {
                            totalPage = totalPage + 1
                        }
                        var backPage = pageNumber - 1;
                        var nextPage = pageNumber + 1;
                        if (pageNumber >= totalPage) {
                            nextPage = totalPage;
                        }
                        if (pageNumber <= 1) {
                            backPage = 1;
                        }

                        $("#pageNumber").append('<li class="prev"><a href="#settings_site#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
                        $("#pageNumber").append('<li class="prev"><a href="#settings_site#' + backPage + '" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');

                        if (totalPage > 6) {

                            if (page != 1) {
                                $("#pageNumber").append('<li><a href="#settings_site#' + (parseInt(page) - 1) + '">...</a></li>');
                            }

                            if (page >= totalPage - 3) {
                                for (var i = (totalPage - 3); i <= totalPage; i++) {
                                    $("#pageNumber").append('<li><a href="#settings_site#' + i + '">' + i + '</a></li>');
                                }
                            } else {
                                for (var i = page; i <= (parseInt(page) + 3); i++) {
                                    $("#pageNumber").append('<li><a href="#settings_site#' + i + '">' + i + '</a></li>');
                                }
                                $("#pageNumber").append('<li><a href="#settings_site#' + (parseInt(page) + 1) + '">...</a></li>');
                            }
                        } else {
                            for (var i = 1; i <= totalPage; i++) {
                                $("#pageNumber").append('<li><a href="#settings_site#' + i + '">' + i + '</a></li>');
                            }
                        }

                        $("#pageNumber").append('<li class="next"><a href="#settings_site#' + nextPage + '" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
                        $("#pageNumber").append('<li class="next"><a href="#settings_site#' + totalPage + '" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
                    }

                    $.each(productRespone.list, function (i, item) {
                        var tmp = '<tr class="odd gradeX"><td>' + item.domainName + '</td>';
                        tmp += '<td>' + item.ipPublic + '</td>';
                        tmp += '<td>' + item.trangThai + '</td>';
                        tmp += '<td>' + item.dviQly + '</td>';
                        tmp += '<td><a class="btn purple edit" data-toggle="modal" href="#large" onclick="editSettingSites(' + item.id + ');">Sửa  <i class="fa fa-edit"></i></a></td>';
                        tmp += '<td><button onclick="deleteSettingSites(' + item.id + ');" class="btn gray delete" attr-data="' + item.id + '">Xóa  <i class="fa fa-trash-o"></i></button></td></tr>';

                        //tmp += '</tr>';

                        $("#tableContent").append(tmp);
                    });
                    $("#loading_view").hide()
                }
            }).fail(function () {
        console.log("loi");
        $("#loading_view").hide();
    });
}

function editSettingSites(e) {
    $.each(dataPage, function (i, item) {
        if (item.id === e) {
            $("#detailSettingSites").show();
            $("#submit").html("Sửa");

            dataRequest = new SettingSitesRequest();
            dataRequest.id = item.id;
            type = "edit";

            $("#dataSiteId").val(item.id);
            $("#dataSiteName").val(item.domainName);
            $("#dataSiteIP").val(item.ipPublic);
            CKEDITOR.instances.dataSiteHeader.setData(item.header);
            CKEDITOR.instances.dataSiteFooter.setData(item.footer);
            setCheckedValue(dataSiteState, item.trangThai);
        }
    });
}

function setCheckedValue(radioObj, newValue) {
    if (!radioObj)
        return;
    var radioLength = radioObj.length;
    if (radioLength == undefined) {
        radioObj.checked = (radioObj.value == newValue.toString());
        return;
    }
    for (var i = 0; i < radioLength; i++) {
        radioObj[i].checked = false;
        if (radioObj[i].value == newValue.toString()) {
            radioObj[i].checked = true;
        }
    }
}

function clickAddNew() {
    dataEdit = null;
    type = "";
    $("#dataSiteId").val(0);
    listProperties = new Array();
    $("#submit").html("Thêm mới");
    $(".modal-title").html("Thêm mới cấu hình site");
}

function deleteSettingSites(e) {
    if (confirm('Are you sure you want to delete this?')) {
        dataRequest = new SettingSitesRequest();
        
        dataRequest.id = e;
        
        $.ajax({
            url: urlDeleteSettingSites,
            type: 'post',
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            data: dataRequest.toJson()
        }).done(function (response, textStatus) {
            setTimeout(function () {
                requestList(parseInt(page), parseInt(limitPage));
            }, 200);
        }).fail(function () {
            console.log("loi");
        });
    }
}

$("#submit").click(function () {
    if (type != "edit") {
        dataRequest = new SettingSitesRequest();
    }

    dataRequest.id = $("#dataSiteId").val();
    dataRequest.domainName = $("#dataSiteName").val();
    dataRequest.ipPublic = $("#dataSiteIP").val();
    dataRequest.header = CKEDITOR.instances.dataSiteHeader.getData();
    dataRequest.footer = CKEDITOR.instances.dataSiteFooter.getData();
    dataRequest.trangThai = parseInt($('input[name=dataSiteState]:checked').val());

    $.ajax({
        url: urlCreateSettingSites,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: dataRequest.toJson()
    }).done(function (response, textStatus) {
        console.log("create CreateSettingSites: ");
        console.log(response);
        $("#closePopup").click();
        setTimeout(function () {
            requestList(parseInt(page), parseInt(limitPage));
        }, 200);
    }).fail(function () {
        console.log("loi");
    });
});

function frmValidate() {
    var val = document.frmDomin.name.value;
    if (/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/.test(val)) {
        alert("Valid Domain Name");
        return true;
    } else {
        alert("Enter Valid Domain Name");
        val.name.focus();
        return false;
    }
}