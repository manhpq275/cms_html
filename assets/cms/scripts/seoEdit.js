$("#category_message").hide();

var SeoRequest = function (){
    this.id                 = -1;
    this.slug               = "";
    this.dviQly             = bussinessCode;
    this.phanLoai           = "";
    this.idLienKet          = "";
    this.title              = "";
    this.description        = "";
    this.keywords           = "";
    this.favico             = "";
    this.ogSiteName         = "";
    this.ogTitle            = "";
    this.ogDescription      = "";
    this.ogType             = "";
    this.ogImage            = "";
    this.ogUrl              = "";
    this.ogLocale           = "";
    this.ogImageWidth       = "";
    this.ogImageHeight      = "";
    this.articleTag         = "";
    this.twitterCard        = "";
    this.twitterDescription = "";
    this.twitterTitle       = "";
    this.twitterImage       = "";

    this.toJson = function (){
        return JSON.stringify(this);
    };
};

var seoObject = new SeoRequest();
var seoTMP = new SeoRequest();
seoObject.phanLoai = 0;
var isCheckLink = false;
$("#submitSEO").click(function(){
  console.log("click seo");
  // seoObject.id            = $("#seoIDPHANLOAI").val();
  // seoObject.phanLoai      = $("#seoIDPHANLOAI").val();
  // seoObject.idLienKet     = $("#seoIDLIENKET").val();
  if (!isCheckLink) {
    $("#seoMessage").html("Bạn chưa nhấn kiểm tra link rút gọn hoặc link rút gọn đã tồn tại");
  } else {
    seoTMP.phanLoai      = $("#seoType").val();
    seoTMP.dviQly        = bussinessCode;
    seoTMP.slug          = $("#seoSlug").val().replaceAll("/","-").replaceAll("(","").replaceAll(")","").replaceAll("---","-").replaceAll("--","-").replaceAll(":","").replaceAll("&","");
    seoTMP.title         = $("#seoTitle").val();
    seoTMP.description   = $("#seoDescription").val();
    seoTMP.keywords      = $("#seoKeyWorks").val();
    seoTMP.favico        = $("#seoFavicon").attr('src');
    seoTMP.ogSiteName    = $("#seoOGSITE").val();
    seoTMP.ogTitle       = $("#seoTitle").val();
    seoTMP.ogDescription = $("#seoDescription").val();
    seoTMP.ogType        = $("#seoOGTYPE").val();
    seoTMP.ogImage       = $("#seoImage").attr('src');
    seoTMP.ogUrl         = $("#seoSlug").val();
    seoTMP.ogLocale      = $("#seoOGLOCALE").val();
    seoTMP.ogImageWidth  = $("#seoOGWIDTH").val();
    seoTMP.ogImageHeight = $("#seoOGHEIGHT").val();
    seoTMP.articleTag    = $("#seoKeyWorks").val();
    seoTMP.twitterCard   = "summary";
    seoTMP.twitterDescription   = $("#seoDescription").val();
    seoTMP.twitterImage         = $("#seoImage").attr('src');
    seoTMP.twitterTitle   = $("#seoTitle").val();
    if (isEmpty(seoTMP.title) || isEmpty(seoTMP.keywords)) {
      $("#seoMessage").html("Không được để trống Tiêu đề hoặc Keywords");
    } else if (seoTMP.description.length > 500 ){
      $("#seoMessage").html("Mô tả SEO tối đa 500 ký tự");
    } else {
        $("#closePopupSEO").click();
        $("#helperSEO").hide();
    }
  }
});

function requestRouteSeo(phanLoai,id, callback){
  isCheckLink = false;
  $.ajax({
    url: urlSeoRequest,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({dviQly:bussinessCode,idLienKet:id, phanLoai:phanLoai})
        }).done(function (response, textStatus){
          if (response.totalResult != 0) {
            seoObject = response.list[0];
            seoTMP = response.list[0];
            console.log(seoObject);
            callback(true);
          } else {
            seoTMP = new SeoRequest();
            callback(false);
          }
     }).fail(function (){
       console.log( "loi requestRouteSeo" );
       callback(false);
       seoTMP = new SeoRequest();
     });

}

function createRouteSeo(seoObject, callback){
  $.ajax({
    url: urlCreatSeoRequest,
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(seoObject)
        }).done(function (response, textStatus){
          console.log(response);
          callback(true);
          seoTMP = new SeoRequest();
     }).fail(function (){
       console.log( "loi" );
       callback(false);
       seoTMP = new SeoRequest();
     });

}

$("#checkSlug").click(function(){
  var slug = $("#seoSlug").val().replaceAll("/","-").replaceAll("(","").replaceAll(")","").replaceAll("---","-").replaceAll("--","-").replaceAll(":","").replaceAll("&","");
  if (isEmpty(slug)) {
      $("#helpSlug").html('Không được để trống link rút gọn</font>');
    } else {
        requestService(urlGetSeoBySlug, JSON.stringify({maDviQly: bussinessCode, slug: $("#seoSlug").val()}), function(data){
          if (data != false) {
            $("#seoMessage").html("");
            if (data.totalResult > 0) {
              if (seoTMP.id !== -1 && data.list[0].id === seoTMP.id) {
                $("#helpSlug").html('<font color="#4b8df8">Bạn có thể sử dụng link: ' + $("#seoSlug").val() + '</font>');
                isCheckLink = true;
              } else {
                $("#helpSlug").html('<font color="red">Link rút gọn đã tồn tại. Vui lòng sửa link rút gọn</font>');
                isCheckLink = false;
              }
            } else {
              $("#helpSlug").html('<font color="#4b8df8">Bạn có thể sử dụng link: ' + $("#seoSlug").val() + '</font>');
              isCheckLink = true;
            }
          }
        });
    }
 
});

$("#seoSlug").on('change input keyup paste', function(){
  var tmp = $("#seoSlug").val().replaceAll(" ","-");
  $("#seoSlug").val(tmp);
});