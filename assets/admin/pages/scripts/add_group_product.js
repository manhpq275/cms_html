$("#productMessage").hide();
var type = $(location).attr('href').split("#")[2];
var dataEdit;

$("#avatarImage").change(function(event){
  var tmppath = URL.createObjectURL(event.target.files[0]);

    $("#avatarProduct").attr("src",tmppath);
});

if (type=== "edit") {
  $("#submit").html("Sửa");
  dataEdit = JSON.parse(localStorage.newsEdit);
  console.log(dataEdit.danhMucCha);
   $("#categoryName").val(dataEdit.ten);
} else {
  $("#submit").html("Thêm mới");
}
$.ajax({
    url: httpBaseRequest + "listNewsCategory",
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({MA_DVI:bussinessCode})
        }).done(function (response, textStatus){
           console.log( response );
          if (response.totalResult == 0) {
            $("#listParent").hide();
            $("#loading_view").hide();
          } else {
            for (var i = 0; i < response.list.length; i++) {
              if(dataEdit == undefined){
                $("#listParent").append(new Option(response.list[i].ten, response.list[i].id));
              } else {
                if(dataEdit.id != response.list[i].id){
                  $("#listParent").append(new Option(response.list[i].ten, response.list[i].id));
                } 
              }
              if (type=== "edit") {
                if (dataEdit.danhMucCha !== undefined){
                  if (dataEdit.danhMucCha.id == response.list[i].id) {
                    $('#listParent > option').eq(i+1).attr('selected','selected');
                  }
                }
              }
            }
            $("#loading_view").hide();
          }
         
     }).fail(function (){
       console.log( "loi" );
       $("#loading_view").hide();
     });

$("#submit").click(function(){
    console.log($("#categoryName").val());
    console.log($("#listParent").val());
    var dataRequest;
    if (type=== "edit") {
      dataRequest = {MA_DVI:bussinessCode , ten: $("#categoryName").val(), danhMucCha: $("#listParent").val(), id:dataEdit.id}
    }else{
      dataRequest = {MA_DVI:bussinessCode , ten: $("#categoryName").val(), danhMucCha: $("#listParent").val()}
    }
    $.ajax({
    url: httpBaseRequest + "createNewsCategory",
    type: 'post',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(dataRequest)
        }).done(function (response, textStatus){
           console.log( response );
          window.location.href = '#news_category';
         
     }).fail(function (){
       console.log( "loi" );
       $("#loading_view").hide();
     });
});
