var urlListComments = httpBaseRequest + "getListComments";

var CommentsRequest = function () {

    this.page = -1;
    this.limit = -1;
    this.cmtCate = 1;
    this.maDviQly = bussinessCode;

    this.toJson = function () {
        return JSON.stringify(this);
    };
}

var totalPage = 0;
var page = $(location).attr('href').split("#")[2];
var limitPage = 50;
var dataPage = new Array();
var productRequest = new CommentsRequest();
var productRespone;

if (page === undefined) {
    page = 1;
    requestList(parseInt(page), parseInt(limitPage), $("#cmtCate option:selected").val());
} else {
    requestList(parseInt(page), parseInt(limitPage), $("#cmtCate option:selected").val());
}

$('#cmtCate').on('change', function() {
    requestList(parseInt(page), parseInt(limitPage), $("#cmtCate option:selected").val());
});

function requestList(pageNumber, limit, search) {
    productRequest = new CommentsRequest();
    productRequest.page = pageNumber;
    productRequest.limit = limit;
    productRequest.cmtCate = search;
    productRequest.maDviQly = bussinessCode;

    $.ajax({
        url: urlListComments,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: productRequest.toJson({page: pageNumber, limit: limit, cmtCate: search, maDviQly: bussinessCode})})
            .done(function (response, textStatus) {
                productRespone = ResponeObject.fromRespone(response);
                console.log(response);

                $("#pageNumber").html("");
                $("#tableContent").html("");

                if (response.totalResult == 0) {
                    $("#loading_view").hide();
                } else {
                    dataPage = productRespone.list;

                    $("#totalResult").html("Hiển thị " + response.list.length + " trên tổng số " + response.totalResult + " khách hàng");

                    if (parseInt(response.totalResult) <= parseInt(limit)) {
                        $("#contentPageX").hide();
                    } else {
                        totalPage = parseInt(response.totalResult / limit);
                        if (response.totalResult % limit !== 0) {
                            totalPage = totalPage + 1
                        }
                        var backPage = pageNumber - 1;
                        var nextPage = pageNumber + 1;
                        if (pageNumber >= totalPage) {
                            nextPage = totalPage;
                        }
                        if (pageNumber <= 1) {
                            backPage = 1;
                        }

                        $("#pageNumber").append('<li class="prev"><a href="#commentList#1" title="Trang đầu"><i class="fa fa-angle-double-left"></i></a></li>');
                        $("#pageNumber").append('<li class="prev"><a href="#commentList#' + backPage + '" title="Trang trước"><i class="fa fa-angle-left"></i></a></li>');

                        if (totalPage > 6) {

                            if (page != 1) {
                                $("#pageNumber").append('<li><a href="#commentList#' + (parseInt(page) - 1) + '">...</a></li>');
                            }

                            if (page >= totalPage - 3) {
                                for (var i = (totalPage - 3); i <= totalPage; i++) {
                                    $("#pageNumber").append('<li><a href="#commentList#' + i + '">' + i + '</a></li>');
                                }
                            } else {
                                for (var i = page; i <= (parseInt(page) + 3); i++) {
                                    $("#pageNumber").append('<li><a href="#commentList#' + i + '">' + i + '</a></li>');
                                }
                                $("#pageNumber").append('<li><a href="#commentList#' + (parseInt(page) + 1) + '">...</a></li>');
                            }
                        } else {
                            for (var i = 1; i <= totalPage; i++) {
                                $("#pageNumber").append('<li><a href="#commentList#' + i + '">' + i + '</a></li>');
                            }
                        }

                        $("#pageNumber").append('<li class="next"><a href="#commentList#' + nextPage + '" title="Trang sau"><i class="fa fa-angle-right"></i></a></li>');
                        $("#pageNumber").append('<li class="next"><a href="#commentList#' + totalPage + '" title="Trang cuối"><i class="fa fa-angle-double-right"></i></a></li>');
                    }

                    $.each(productRespone.list, function (i, item) {
                        var tmp = '<tr class="odd gradeX"><td>' + item.ten + '</td>';
                        tmp += '<td>' + item.maDviQly + '</td>';
                        tmp += '<td>' + item.binhChon + '</td>';
                        tmp += '<td>' + (item.hinhAnh=== undefined ? "" : '<img src="'+item.hinhAnh+'" width="120" height="120"/>') + '</td>';
                        tmp += '<td>' + 
                                ((item.kiemDuyet == 0) ? "<i class='glyphicon glyphicon-remove'style='color:red;'></i>" : 
                                "<i class='glyphicon glyphicon-ok' style='color:green;'></i>")
                             + '</td>';
                        tmp += '<td><a class="btn purple edit" data-toggle="modal" href="#large" onclick="editComment(' + item.id + ');">Duyệt bài  <i class="fa fa-edit"></i></a></td></tr>';

                        $("#tableContent").append(tmp);
                    });
                    
                    $("#loading_view").hide();
                }
            }).fail(function () {
        console.log("loi");
        $("#loading_view").hide();
    });
}

